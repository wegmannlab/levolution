/*
 * someMath.h
 *
 *  Created on: Jun 3, 2011
 *      Author: wegmannd
 */

#ifndef SOMEMATH_H_
#define SOMEMATH_H_

#include <math.h>
#include <algorithm>



double poisson(const double & lambda, const int n){
	return exp(-lambda) * pow(lambda, n) / factorial(n);
}


#endif /* SOMEMATH_H_ */
