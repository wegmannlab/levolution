/*
 * TTree.cpp
 *
 *  Created on: Feb 28, 2009
 *      Author: wegmannd
 */

#include "TTree.h"
//---------------------------------------------------------------
long factorial(const int & x){
	if (x==1) return 1;
	int fac = 1;
	for (int i=2; i<=x; ++i) fac*=i;
	return fac;
};

//---------------------------------------------------------------

//------------------------------------------------
// TBaseNode
//------------------------------------------------
std::string TBaseNode::getNewick(){
	std::ostringstream tos;
	tos << distance;
	if(children.size()>0){
		std::string newick="(";
		for(std::vector<TBaseNode*>::size_type i=0; i<children.size(); ++i){
			if(i>0) newick+=",";
			newick+=children[i]->getNewick();
		}
		return newick+"):"+tos.str();
	} else {
		return (std::string) name.c_str()+":"+tos.str();
	}
};

std::string TBaseNode::getNewickValues(){
	std::ostringstream tos;
	tos << value;
	if(children.size()>0){
		std::string newick="(";
		for(std::vector<TBaseNode*>::size_type i=0; i<children.size(); ++i){
			if(i>0) newick+=",";
			newick+=children[i]->getNewickValues();
		}
		return newick+"):"+tos.str();
	} else {
		return (std::string) name.c_str()+":"+tos.str();
	}
};

std::string TBaseNode::getNewickJumps(){
	std::ostringstream tos;
	tos << jumps;
	if(children.size()>0){
		std::string newick="(";
		for(std::vector<TBaseNode*>::size_type i=0; i<children.size(); ++i){
			if(i>0) newick+=",";
			newick+=children[i]->getNewickJumps();
		}
		return newick+"):"+tos.str();
	} else {
		return (std::string) name.c_str()+":"+tos.str();
	}
};

void TBaseNode::sampleJumps(const double lambda){
	jumps = coretools::instances::randomGenerator().getPoissonRandom(lambda*distance);
};

void TBaseNode::simulateJumpEvolution(const double poissonVar){
	value = coretools::instances::randomGenerator().getNormalRandom(0, sqrt(jumps*poissonVar));
	trait += value;
}

void TBaseNode::simulateTraitEvolution(const double brownianVar){
	trait += coretools::instances::randomGenerator().getNormalRandom(0, sqrt(brownianVar*distance));
}


//------------------------------------------------
// TNode
//------------------------------------------------

void TBaseNode::propagateBrownianTrait(const double Trait, const double brownianVar){
	trait = Trait;
	simulateTraitEvolution(brownianVar);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
		(*i)->propagateBrownianTrait(trait, brownianVar);
	}
};

void TBaseNode::propagateLevyTrait(const double Trait, const double brownianVar, const double poissonVar, const double lambda){
	trait = Trait;
	simulateTraitEvolution(brownianVar);
	sampleJumps(lambda);
	simulateJumpEvolution(poissonVar);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
		(*i)->propagateLevyTrait(trait, brownianVar, poissonVar, lambda);
	}
};

void TBaseNode::propagateLevyTraitExisitingJumps(const double Trait, const double brownianVar, const double poissonVar){
	//assumes jumps are set per node
	trait = Trait;
	simulateTraitEvolution(brownianVar);
	simulateJumpEvolution(poissonVar);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
		(*i)->propagateLevyTraitExisitingJumps(trait, brownianVar, poissonVar);
	}
};

void TBaseNode::propagateLevyTraitFixedNumJumps(const double Trait, const double brownianVar, const double poissonVar, double & cumulLength, std::vector<double> & waitingTimes, std::vector<double>::iterator & waitingTimesIt){
	cumulLength += distance;
	jumps = 0;
	while(waitingTimesIt!=waitingTimes.end() && *waitingTimesIt < cumulLength){
		++jumps;
		++waitingTimesIt;
	}

	trait = Trait;
	simulateTraitEvolution(brownianVar);
	simulateJumpEvolution(poissonVar);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i)
		(*i)->propagateLevyTraitFixedNumJumps(trait, brownianVar, poissonVar, cumulLength, waitingTimes, waitingTimesIt);
};

//------------------------------------------------
// TRoot
//------------------------------------------------
void TRoot::saveAllParents(){
	std::vector<TBaseNode*> p;
	p.push_back(this);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
		(*i)->saveAllParents(p);
	}
};

void TNode::saveAllParents(std::vector<TBaseNode*> p){
	allParents=p;
	p.push_back(this);
	for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
		(*i)->saveAllParents(p);
	}
};

bool TNode::isParent(TBaseNode* other){
	if(other==this) return true;
	for(std::vector<TBaseNode*>::iterator i=allParents.begin(); i!=allParents.end(); ++i){
		if((*i)==other) return true;
	}
	return false;
};

TBaseNode* TNode::getCommonParent(TBaseNode* other){
	if(other->isParent(this)) return this;
	if(isParent(other)) return other;
	for(std::vector<TBaseNode*>::reverse_iterator i=allParents.rbegin(); i!=allParents.rend(); ++i){
		if(other->isParent(*i)) return (*i);
	}
	throw "No common parent found!";
};

double TNode::getCommonBranchLength(TBaseNode* other){
	TBaseNode* p=getCommonParent(other);
	return p->getDistanceToRoot();
};

int TBaseNode::computeNumDescendantLeaves(){
	if(children.size()==0) numDescendantLeaves=1;
	else {
		numDescendantLeaves=0;
		for(std::vector<TBaseNode*>::iterator i=children.begin(); i!=children.end(); ++i){
			numDescendantLeaves+=(*i)->computeNumDescendantLeaves();
		}
	}
	return numDescendantLeaves;
};

double TNode::getCumulativeValueCommonBranches(TBaseNode* other){
	TBaseNode* p=getCommonParent(other);
	return p->getCumulativeValueToRoot();
};

//---------------------------------------------------------------
// TTree
//---------------------------------------------------------------
TTree::TTree(std::string Newick){
	initialize(Newick, 1);
};

TTree::TTree(std::string Newick, double Probability) {
	initialize(Newick, Probability);
};

void TTree::initialize(const std::string & Newick, const double & Probability){
	newick = Newick;
	probability = Probability;
	distancesKnown = true; //will be set to false if a single distance is missing
	traitMeanComputed = false;
	traitVarComputed = false;
	allParentsSaved = false;
	numDescendantLeavesComputed = false;

	//construct Tree from Nexus....
	coretools::str::eraseAllWhiteSpaces(newick);
	if(newick[0]!='(') throw "This tree is not in proper format:\n'" + newick+ "'!";
	makeChildNodes(newick);

	//compute total tree length
	_computeTotalTreeLength();
};

TTree::TTree(int & numSpecies, double birthrate, double deathrate, double age){
	//this function generates a new tree randomly conditioning on those parameters
	//modified from the R function by Tanja Stadler

	std::map<int, TBaseNode*> leaves_temp;

	//generate root
	root=new TRoot();
	nodes.push_back(root);
	//special case if there is only one species in the tree
	if(numSpecies==1){
		root->setDistance(age);
		leaves.insert(std::pair<std::string, TBaseNode*>("root", root));
	} else {
		//first generate a list of speciation events
		std::vector<double> specEvents;
		specEvents.push_back(age);
		if(birthrate>deathrate){
			double diff=birthrate-deathrate;
			double AA = 1 - exp(-diff*age);
			double YY = birthrate-deathrate*(1-AA);
			for(int i=0; i<(numSpecies-1); ++i){
				double r = AA*coretools::instances::randomGenerator().getRand();
				specEvents.push_back((1/diff) * log((YY-deathrate*r)/(YY-birthrate*r)));
			}
		} else {
			for(int i=0; i<(numSpecies-1); ++i){
				double r=coretools::instances::randomGenerator().getRand();
				specEvents.push_back((age*r)/(1+birthrate*age*(1+r)));
			}
		}
		sort(specEvents.begin(), specEvents.end());

		std::vector<int> potentialLeaves;
		std::map<int, double> creationTime;
		potentialLeaves.push_back(0);
		creationTime.insert(std::pair<int,double>(0, age));

		int nextNode=1;
		for(int i=specEvents.size()-2; i>=0; --i){
			int leafNum = coretools::instances::randomGenerator().getRand<size_t>(0, potentialLeaves.size());
			int x=potentialLeaves[leafNum];
			nodes[x]->setDistance(creationTime.find(x)->second-specEvents[i]);
			nodes.push_back(new TNode(nodes[x], -1));
			nodes.push_back(new TNode(nodes[x], -1));
			nodes[x]->addChild(nodes[nextNode]);
			nodes[x]->addChild(nodes[nextNode+1]);
			creationTime.insert(std::pair<int,double>(nextNode, specEvents[i]));
			creationTime.insert(std::pair<int,double>(nextNode+1, specEvents[i]));
			potentialLeaves[leafNum]=nextNode;
			potentialLeaves.push_back(nextNode+1);
			nextNode+=2;
		}
		for(unsigned int i=0; i<nodes.size(); ++i){
			if(nodes[i]->getDistance()==-1){
				nodes[i]->setDistance(creationTime.find(i)->second);
				leaves_temp.insert(std::pair<int, TBaseNode*>(i, nodes[i]));
			}
		}
	}
	//reset leave names
	int num=0;
	std::string name;
	for(std::map<int, TBaseNode*>::iterator i=leaves_temp.begin(); i!=leaves_temp.end(); ++i, ++num){
		name="Leaf_" + coretools::str::toString(num);
		leaves.insert(std::pair<std::string, TBaseNode*>(name, i->second));
		i->second->setName(name);
	}

	//compute total tree length
	_computeTotalTreeLength();

	//set some variables
	probability = 1.0;
	distancesKnown = true;
	traitMeanComputed = false;
	traitVarComputed = false;
	allParentsSaved = false;
	numDescendantLeavesComputed = false;
	traitMean = -1.0;
	traitVar = -1.0;
};

TTree::TTree(const TTree & other){
	initialize(other.newick, other.probability);

	//set others
	traitMean = other.traitMean;
	traitVar = other.traitVar;
	totLength = other.totLength;
	traitMeanComputed = other.traitMeanComputed;
	traitVarComputed = other.traitVarComputed;
};

void TTree::_computeTotalTreeLength(){
	totLength = 0.0;
	for(unsigned int i=0; i<nodes.size(); ++i){
		totLength += nodes[i]->getDistance();
	}
};

//---------------------------------------------------------------
void TTree::makeChildNodes(std::string nexusSnipplet, TBaseNode* parent){
	//extract what is specific for this node
	std::string name;

	//name=nexusSnipplet.substr(nexusSnipplet.find_last_of(')')+1);
	name=nexusSnipplet.substr(nexusSnipplet.find_last_of(')')+1);
	double dist;

	if(coretools::str::stringContains(name, ':')){
		dist = coretools::str::convertString<double>(coretools::str::extractAfter(name, ':'));
		name = coretools::str::extractBefore(name,':');
		coretools::str::trimString(name);
	} else {
		dist=0;
		name.clear();
	}

	//make node
	TBaseNode* newParent;
	if(parent==NULL){
		//the first element is always the top and has no name
		root=new TRoot(dist);
		nodes.push_back(root);
		newParent=root;
	} else {
		nodes.push_back(new TNode(parent, dist, name));
		newParent=nodes[nodes.size()-1];
		parent->addChild(newParent);
	}

	if(nexusSnipplet[0]=='('){
		//has childes....
		int i=1;
		int openBrackets=0;
		int start=1;
		for(;;){
			if(openBrackets==0 && (nexusSnipplet[i]==',' || nexusSnipplet[i]==')')){
				//we have a child!
				//makeChildNodes(nexusSnipplet.extract_between(start, i), newParent);
				makeChildNodes(nexusSnipplet.substr(start, i-start), newParent);
				if(nexusSnipplet[i]==')') break;
				else start=i+1;
			}
			if(nexusSnipplet[i]=='(') ++openBrackets;
			if(nexusSnipplet[i]==')') --openBrackets;
			if(nexusSnipplet[i]==';') break;
			if(nexusSnipplet[i]=='\0') break;
			++i;
		}
		if(openBrackets!=0) throw "This tree is not in proper format (uneven opening and closing brackets):\n'" + newick+ "'!";
	} else {
		//is a leave!
		if(name.empty()) throw "Leaf without name in tree:\n'" + newick+ "'!";
		leaves.insert(std::pair<std::string, TBaseNode*>(name, nodes[nodes.size()-1]));
	}
};

uint16_t TTree::getNumLeaves() const{
	return leaves.size();
};

uint16_t TTree::getNumNodes() const{
	return nodes.size();
};

TBaseNode* TTree::getLeaveFromName(std::string name){
	//this function searches the name among the leaves and returns a pointer to that leaf
	std::map<std::string, TBaseNode*>::iterator leaf=leaves.find(name);
	if(leaf==leaves.end()) throw "Leaf '" +name+"' is missing from tree!";
	return leaf->second;
};

uint16_t TTree::getLeaveNumberFromName(const std::string & name) const{
	//this function searches the name among the leaves and returns a pointer to that leaf
	auto leaf = leaves.find(name);
	if(leaf==leaves.end()) throw "Leaf '" +name+"' is missing from tree!";
	int num=0;
	for(std::map<std::string, TBaseNode*>::const_iterator i=leaves.begin(); i!=leaves.end(); ++i, ++num){
		if(i->second==leaf->second) return num;
	}
	return 0;
};

TBaseNode* TTree::getParentOfNodeFromName(std::string name){
	//this function searches the name among the leaves and returns a pointer to that leaf
	std::map<std::string, TBaseNode*>::iterator leaf=leaves.find(name);
	if(leaf==leaves.end()) throw "Leaf '" +name+"' is missing from tree!";
	return leaf->second->getParent();
};

double TTree::getBranchLengthFromName(std::string name){
	//this function searches the name among the leaves and returns a pointer to that leaf
	std::map<std::string, TBaseNode*>::iterator leaf=leaves.find(name);
	if(leaf==leaves.end()) throw "Leaf '" +name+"' is missing from tree!";
	return leaf->second->getDistance();
}

std::string TTree::getNewick(){
	return root->getNewick()+";";
};

std::string TTree::getNewickValues(){
	return root->getNewickValues()+";";
};

std::string TTree::getNewickJumps(){
	return root->getNewickJumps()+";";
};

void TTree::evoloveTrait(double rootState, double brownianVariance){
	//start at root and propagate down
	root->propagateBrownianTrait(rootState, brownianVariance);
};

void TTree::evoloveTraitLevy(double rootState, double brownianVariance, double poissonVariance, double lambda){
	//start at root and propagate down
	root->propagateLevyTrait(rootState, brownianVariance, poissonVariance, lambda);
};

void TTree::evolveTraitLevyExisitingJumps(const double rootState, const double brownianVariance, const double poissonVariance){
	//start at root and propagate down
	root->propagateLevyTraitExisitingJumps(rootState, brownianVariance, poissonVariance);
};

void TTree::evolveTraitLevyFixedNumJumps(double rootState, double brownianVariance, double poissonVariance, int numJumps){
	if(numJumps < 1){
		//simulate without jumps
		evoloveTrait(rootState, brownianVariance);
	} else {
		//just place them uniformly on total tree
		std::vector<double> waitingTimes;
		std::vector<double>::iterator waitingTimesIt;
		double cumulLength;

		getTotalTreeLength();
		for(int i=0; i<numJumps; ++i){
			waitingTimes.push_back(coretools::instances::randomGenerator().getRand() * totLength);
		}
		std::sort(waitingTimes.begin(), waitingTimes.end());

		waitingTimesIt = waitingTimes.begin();
		root->propagateLevyTraitFixedNumJumps(rootState, brownianVariance, poissonVariance, cumulLength, waitingTimes, waitingTimesIt);

	}
};

double TTree::getTraitValue(int num){
	std::map<std::string, TBaseNode*>::iterator i=leaves.begin();
	for(int j=0; j<num; ++j) ++i;
	return i->second->getTrait();
};

double TTree::getTraitMean(){
	//computes the mean trait across all leaves
	if(!traitMeanComputed){
		traitMean=0;
		for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i){
			traitMean+=i->second->getTrait();
		}
		traitMean/=leaves.size();
	}
	return traitMean;
};

double TTree::getTraitVariance(){
	//computes the mean trait across all leaves
	if(!traitVarComputed){
		if(leaves.size()==1) traitVar=-1;
		else {
			getTraitMean();
			traitVar=0;
			for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i){
				traitVar+=(i->second->getTrait()-traitMean)*(i->second->getTrait()-traitMean);
			}
			traitVar/=(leaves.size()-1);
		}
	}
	return traitVar;
};

double TTree::getCommonBranchLength(int i, int j){
	//save all parents in nodes for quick lookup
	if(!allParentsSaved){
		root->saveAllParents();
		allParentsSaved=true;
	}
	//computes the sum of all branches common to nodes i and j when looking from the root
	std::map<std::string, TBaseNode*>::iterator leaveI=leaves.begin();
	std::map<std::string, TBaseNode*>::iterator leaveJ=leaves.begin();
	advance(leaveI, i);
	advance(leaveJ, j);
	return leaveI->second->getCommonBranchLength(leaveJ->second);
};

void TTree::setValue(double Val){
	for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i){
		(*i)->setValue(Val);
	}
};

void TTree::setJumps(int jumps){
	for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i){
		(*i)->setJumps(jumps);
	}
};

void TTree::sampleValuePoisson(double & lambda){
	for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i)
		(*i)->simulateJumpEvolution(lambda);
};

double TTree::getCumulativeValueCommonBranches(int i, int j){
	//save all parents in nodes for quick lookup
	if(!allParentsSaved){
		root->saveAllParents();
		allParentsSaved=true;
	}
	//computes the sum of all branches common to nodes i and j when looking from the root
	std::map<std::string, TBaseNode*>::iterator leaveI=leaves.begin();
	std::map<std::string, TBaseNode*>::iterator leaveJ=leaves.begin();
	advance(leaveI, i);
	advance(leaveJ, j);
	return leaveI->second->getCumulativeValueCommonBranches(leaveJ->second);
};

double TTree::getValueOneBranch(int branch){
	return nodes[branch]->getValue();
};

double TTree::getCumulativeValue(){
	double tot = 0.0;
	for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i){
		tot += (*i)->value;
	}
	return tot;
}
int TTree::getNumJumps(){
	int tot = 0.0;
	for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i){
		tot += (*i)->jumps;
	}
	return tot;
};

void TTree::setValueOfBranch(int branch, double Val){
	nodes[branch]->value = Val;
};

void TTree::addValueToBranch(int branch, double Val){
	nodes[branch]->value += Val;
;}

void TTree::setNumJumpsOnBranch(int branch, int jumps){
	nodes[branch]->jumps += jumps;
};

bool TTree::isSubordinate(int node, int leave){
	if(!allParentsSaved){
		root->saveAllParents();
		allParentsSaved=true;
	}
	std::map<std::string, TBaseNode*>::iterator leaveI=leaves.begin();
	advance(leaveI, leave);
	return leaveI->second->isParent(nodes[node]);
};

int TTree::getNumDescendantLeaves(int node){
	if(!numDescendantLeavesComputed){
		root->computeNumDescendantLeaves();
		numDescendantLeavesComputed=true;
	}
	return nodes[node]->getNumDescendantLeaves();
};

double TTree::getTotalTreeLength() const{
	return totLength;
};

void TTree::scaleTree(double length){
	double scale = length / totLength;
	for(unsigned int i=0; i<nodes.size(); ++i)
		nodes[i]->scale(scale);
	totLength = length;
};

std::string TTree::getLeaveName(int leave) const{
	auto leaveI = leaves.begin();
	advance(leaveI, leave);
	return leaveI->first;
};

double TTree::getLeaveTrait(int leave) const{
	auto leaveI = leaves.begin();
	advance(leaveI, leave);
	return leaveI->second->getTrait();
};

std::vector<double>  TTree::getLeaveTraits(){
	std::vector<double> x(leaves.size());
	size_t idx = 0;
	for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i, ++idx){
		x[idx] = i->second->getTrait();
	}
	return x;
};

void TTree::printLeaveNamesToSTDout(std::string delim){
	std::map<std::string, TBaseNode*>::iterator i=leaves.begin();
	std::cout << i->first; ++i;
	for(; i!=leaves.end(); ++i) std::cout << delim << i->first;
};

void TTree::printLeaveTraitValuesToSTDout(std::string delim){
	std::map<std::string, TBaseNode*>::iterator i=leaves.begin();
	std::cout << i->second->getTrait(); ++i;
	for(; i!=leaves.end(); ++i) std::cout << delim << i->second->getTrait();
};

void TTree::printLeaveNameTraitTable(std::string delim){
	for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i)
		std::cout << i->first << delim << i->second->getTrait() << std::endl;
};

void TTree::printLeaveNameTraitTable(std::ofstream & out, std::string delim){
	for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i)
		out << i->first << delim << i->second->getTrait() << std::endl;
};

void TTree::printLeaveNameTraitTableWithPrefix(std::string prefix, std::string delim){
	for(std::map<std::string, TBaseNode*>::iterator i=leaves.begin(); i!=leaves.end(); ++i)
		std::cout << prefix << i->first << delim << i->second->getTrait() << std::endl;
};

int TTree::getRandomNodeWithBranchlengthWeights(){
	long neededLength=coretools::instances::randomGenerator().getRand() * getTotalTreeLength();
	long sum = 0;
	for(unsigned int i = 0; i<nodes.size(); ++i){
		sum+=nodes[i]->getDistance();
		if(sum>neededLength) return i;
	}
	return nodes.size()-1;
};

int TTree::getRandomNode(){
	return coretools::instances::randomGenerator().getRand<size_t>(0, nodes.size());
};

bool TTree::hasPolytomy(){
	for(unsigned int i=0; i<nodes.size(); ++i){
		if(nodes[i]->children.size() > 2) return true;
	}
	return false;
};





