/*
 * TLevyParams.h
 *
 *  Created on: Jan 24, 2022
 *      Author: phaentu
 */

#ifndef CORE_TLEVYPARAMS_H_
#define CORE_TLEVYPARAMS_H_

#include "coretools/Main/TParameters.h"
#include "TVariables.h"

//-------------------------------------
// TModelParamsBase
//-------------------------------------
class TModelParamsBase{
protected:
	double _logLikelihhood{0.0};
	std::vector<double> _brownianVariances;
	std::vector<double> _rootStates;
	bool _modelFitted{false};

	void _setFromParameters(const std::string & ParamName, const std::string & ParamNameFull, std::vector<double> & Param, size_t Dimension);
	void _setFromParameters(const std::string & ParamName, const std::string & ParamNameFull, std::vector<double> & Param, const std::vector<double> & NullModelMLE);

public:
	TModelParamsBase() = default;
	virtual ~TModelParamsBase() = default;

	double logLikelihood() const;
	const std::vector<double>& rootStates() const;
	const std::vector<double>& brownianVariances() const;

	std::vector<double>& rootStates();
	std::vector<double>& brownianVariances();

	void setRootStates(std::vector<double> & RootStates);
	void setBrownianVariance(std::vector<double> & BrownianVariances);

	void setRootStatesBrownianVariancesFromParameters(size_t Dimension);
	void setRootStatesFromParameters(const std::vector<double> & NullModelMLE);
	void setBrownianVariancesFromParameters(const std::vector<double> & NullModelMLE);

	virtual void addParameterNames(std::vector<std::string> & Names, const std::string & Prefix = "") const;
	virtual void write(coretools::TOutputFile & Out) const;

	virtual void report() const;
};

//-------------------------------------
// TNullModel
//-------------------------------------
class TNullModelParams:public TModelParamsBase{
public:
	TNullModelParams() = default;
	~TNullModelParams() = default;

	void fit(TVariables* Vars);
};

//------------------------------------------------
// TlevyParams
//------------------------------------------------
class TlevyParams:public TModelParamsBase{
private:
	double _lambda{0.0};
	double _alpha{0.0};
	double _FisherInfo{0.0};

public:
	TlevyParams();
	~TlevyParams() = default;

	TlevyParams(double Alpha);
	TlevyParams(size_t Dimension);

	double FisherInfo() const;
	double alpha() const;
	double lambda() const;

	void setAlpha(double Alpha);
	void setLambda(double Lambda);
	void setModelfit(double LogLikelihood, double FisherInfo);

	void setLambdaFromParameters(const TTree & Tree);
	void setFromParameters(size_t Dimension);

	void addParameterNames(std::vector<std::string> & Names, const std::string & Prefix = "", bool includeAlpha = true) const;
	void write(coretools::TOutputFile & Out) const;

	void report() const;
	void writeHeaderToLogTable() const;
	void writeToLogTable() const;
};

//------------------------------------------------
// TModelChoice
//------------------------------------------------
class TModelChoice{
private:
	double _nullModelLikelihood;
	double _levyLikelihood {0.0};
	double _LRT_statistics {0.0};
	double _LRT_pValue {0.0};
	double _AIC_difference{ 0.0};

	double chisq[145]={0.0, 0.2107210, 0.4462871, 0.7133499, 1.0216512, 1.3862944, 1.8325815, 2.4079456, 3.2188758, 4.6051702, 4.8158912, 5.0514573, 5.3185201, 5.6268214, 5.9914645, 6.4377516, 7.0131158, 7.8240460, 9.2103404, 9.4210614, 9.6566275, 9.9236903, 10.2319916, 10.5966347, 11.0429218, 11.6182860, 12.4292162, 13.8155106, 14.0262316, 14.2617977, 14.5288604, 14.8371618, 15.2018049, 15.6480920, 16.2234562, 17.0343864, 18.4206807, 18.6314018, 18.8669678, 19.1340306, 19.4423320, 19.8069751, 20.2532622, 20.8286264, 21.6395566, 23.0258509, 23.2365720, 23.4721380, 23.7392008, 24.0475022, 24.4121453, 24.8584324, 25.4337965, 26.2447268, 27.6310211, 27.8417421, 28.0773082, 28.3443710, 28.6526724, 29.0173155, 29.4636026, 30.0389667, 30.8498969, 32.2361913, 32.4469123, 32.6824784, 32.9495412, 33.2578426, 33.6224857, 34.0687728, 34.6441369, 35.4550671, 36.8413615, 37.0520825, 37.2876486, 37.5547114, 37.8630127, 38.2276559, 38.6739430, 39.2493071, 40.0602373, 41.4465317, 41.6572528, 41.8928189, 42.1598817, 42.4681828, 42.8328259, 43.2791130, 43.8544771, 44.6654073, 46.0517017, 46.2624227, 46.4979888, 46.7650516, 47.0733529, 47.4379961, 47.8842832, 48.4596473, 49.2705775, 50.6568719, 50.8675880, 51.1031479, 51.3702345, 51.6785305, 52.0431662, 52.4894422, 53.0647879, 53.8757921, 55.2620865, 55.4728815, 55.7082626, 55.9754046, 56.2838117, 56.6481588, 57.0945569, 57.6701061, 58.4814064, 59.8665906, 60.0770650, 60.3123229, 60.5789894, 60.8904629, 61.2551060, 61.7013931, 62.2767572, 63.0876874, 64.4739818, 64.6847028, 64.9202689, 65.1873317, 65.4956330, 65.8602762, 66.3065633, 66.8819274, 67.6928576, 69.0791520, 69.3147181, 69.5817808, 69.8900822, 70.2547253, 70.2547253, 70.7010124, 71.2763766, 72.0873068, 73.4736011};
	double pvalue[145]={1.0, 9e-01, 8e-01, 7e-01, 6e-01, 5e-01, 4e-01, 3e-01, 2e-01, 1e-01, 9e-02, 8e-02, 7e-02, 6e-02, 5e-02, 4e-02, 3e-02, 2e-02, 1e-02, 9e-03, 8e-03, 7e-03, 6e-03, 5e-03, 4e-03, 3e-03, 2e-03, 1e-03, 9e-04, 8e-04, 7e-04, 6e-04, 5e-04, 4e-04, 3e-04, 2e-04, 1e-04, 9e-05, 8e-05, 7e-05, 6e-05, 5e-05, 4e-05, 3e-05, 2e-05, 1e-05, 9e-06, 8e-06, 7e-06, 6e-06, 5e-06, 4e-06, 3e-06, 2e-06, 1e-06, 9e-07, 8e-07, 7e-07, 6e-07, 5e-07, 4e-07, 3e-07, 2e-07, 1e-07, 9e-08, 8e-08, 7e-08, 6e-08, 5e-08, 4e-08, 3e-08, 2e-08, 1e-08, 9e-09, 8e-09, 7e-09, 6e-09, 5e-09, 4e-09, 3e-09, 2e-09, 1e-09, 9e-10, 8e-10, 7e-10, 6e-10, 5e-10, 4e-10, 3e-10, 2e-10, 1e-10, 9e-11, 8e-11, 7e-11, 6e-11, 5e-11, 4e-11, 3e-11, 2e-11, 1e-11, 9e-12, 8e-12, 7e-12, 6e-12, 5e-12, 4e-12, 3e-12, 2e-12, 1e-12, 9e-13, 8e-13, 7e-13, 6e-13, 5e-13, 4e-13, 3e-13, 2e-13, 1e-13, 9e-14, 8e-14, 7e-14, 6e-14, 5e-14, 4e-14, 3e-14, 2e-14, 1e-14, 9e-15, 8e-15, 7e-15, 6e-15, 5e-15, 4e-15, 3e-15, 2e-15, 1e-15, 9e-16, 8e-16, 7e-16, 6e-16, 5e-16, 4e-16, 3e-16, 2e-16, 1e-16};

	std::string _choice(bool IsLevy) const;
	void _reportChoice(bool IsLevy) const;

public:
	TModelChoice(const TNullModelParams & NullModel);

	void performModelchoice(const TlevyParams & LevyParams);
	void performModelchoiceAndReport(const TlevyParams & LevyParams);
	void convergedAtNullModel();

	void addParameterNames(std::vector<std::string> & Names) const;
	void write(coretools::TOutputFile & Out) const;

	void report() const;
};





#endif /* CORE_TLEVYPARAMS_H_ */
