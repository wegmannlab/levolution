/*
 * levolution_core.h
 *
 *  Created on: Jun 1, 2011
 *      Author: wegmannd
 */

#ifndef LEVOLUTION_CORE_H_
#define LEVOLUTION_CORE_H_

#include "TTree.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Main/TParameters.h"
#include "TSigmaN.h"
#include <time.h>
#include "TConvergenceTest.h"
#include <math.h>
#include "TMCMC.h"
#include "coretools/Main/TTask.h"
#include "coretools/Files/TOutputFile.h"


//------------------------------------------------
// TEMIterationOutput
//------------------------------------------------
class TEMIterationOutput{
private:
	coretools::TOutputFile _out;
	uint16_t _iteration;

public:
	TEMIterationOutput();
	TEMIterationOutput(std::string & Filename, const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests);

	void open(std::string & Filename, const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests);

	void write(const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests);
	void close();
};

//------------------------------------------------
// TEMOutput
//------------------------------------------------
class TEMOutput{
private:
	coretools::TOutputFile _out;

public:
	TEMOutput(std::string & Filename, const TlevyParams & LevyParams, const TNullModelParams & NullModelParams, const TModelChoice & ModelChoice);

	void write(const TlevyParams & LevyParams, const TNullModelParams & NullModelParams, const TModelChoice & ModelChoice, const std::string & TreeFile, double TreeLength, const std::string & TraitFile);
	void close();
};

//------------------------------------------------
// TLevolution_core
//------------------------------------------------
class TLevolution_core{
private:
	TTree* tree;
	TVariables* vars;
	TNullModelParams _nullModel;
	double maxBrownVar;
	double maxLambda;
	double minPoisProb;
	int maxNumJumps;
	int numProposals;
	int iterations;
	int maxIterations;
	TMCMCSettings MCMCSettings, MCMCSettingsLL, MCMCSettingsEB;
	double* params;
	bool doSims;
	bool varsInitialized;
	bool treeInitialized;
	double bestEStepWeight;
	TConvergenceTestVector* trendCheckVector;
	bool trendCheckInitialized;
	coretools::TOutputFile EMout; //writes results of each EM step, one per alpha

	std::string treeFile, traitFile;
	std::string outname;
	std::vector<TSigmaN*> SigmaNvec;
	TSigmaN* bestSigmaNvector;

	void computeNullModelMLE();
	void readNewickFromFile(std::string filename, std::string & newick);
	void constructTreeFromFile(std::string filename, TTree* storage);
	void constructTreeFromFile();
	void readAlphaString(std::vector<double> & alphaValues);
	void estimateLocationOfJumps(TlevyParams & levyParams);
	void prepareEM();
	void runEM(TlevyParams & LevyParams, TModelChoice & ModelChoice, TEMOutput & EMOutput, bool WriteEM);

public:
	TLevolution_core();
	virtual ~TLevolution_core(){
		if(treeInitialized) delete tree;
		if(varsInitialized) delete vars;
		for(std::vector<TSigmaN*>::iterator i=SigmaNvec.begin(); i!=SigmaNvec.end(); ++i) delete (*i);
		if(trendCheckInitialized) delete trendCheckVector;
	};

	void simulateTree();
	void performSimulations();
	void estimate();
	void estimateLocationOfJumps();
	void computeLLSurface(TlevyParams & levyParams);
	void estimateLLOnly();
	void performFTest();
	void testMCMCConvergence();
};

//--------------------------------------
// Tasks
//--------------------------------------
class TTask_infer:public coretools::TTask{
public:
	TTask_infer(){ _explanation = "Inferring Lévy parameters for a given tree and traits."; };

	void run(){
		TLevolution_core levolution;
		levolution.estimate();
	};
};

class TTask_LL:public coretools::TTask{
public:
	TTask_LL(){ _explanation = "Calculating the log-likelihood at specific parameter values."; };

	void run(){
		TLevolution_core levolution;
		levolution.estimateLLOnly();
	};
};

class TTask_simulate:public coretools::TTask{
public:
	TTask_simulate(){ _explanation = "Simulating Lévy processes on trees."; };

	void run(){
		TLevolution_core levolution;
		levolution.performSimulations();
	};
};

class TTask_FTest:public coretools::TTask{
public:
	TTask_FTest(){ _explanation = "Testing for particular Lévy jumps using an F-test."; };

	void run(){
		TLevolution_core levolution;
		levolution.performFTest();
	};
};

class TTask_inferJumps:public coretools::TTask{
public:
	TTask_inferJumps(){ _explanation = "Inferring the location of Lévy jumps."; };

	void run(){
		TLevolution_core levolution;
		levolution.estimateLocationOfJumps();
	};
};

class TTask_testMCMC:public coretools::TTask{
public:
	TTask_testMCMC(){ _explanation = "Testing MCMC convergence."; };

	void run(){
		TLevolution_core levolution;
		levolution.testMCMCConvergence();
	};
};

#endif /* LEVOLUTION_CORE_H_ */
