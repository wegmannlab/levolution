/*
 * TMCMC.cpp
 *
 *  Created on: Sep 16, 2014
 *      Author: wegmannd
 */

#include "TMCMC.h"

//-------------------------------------
// TMCMCSettings
//-------------------------------------
void TMCMCSettings::_setTotalLength(){
	_totalLength = _burninLength + _numNVectors * _thinning;
};

void TMCMCSettings::initialize(const std::string & Postfix, uint32_t DefaultNumNVectors, uint32_t DefaultThinning, uint32_t DefaultBurninLength){
	_numNVectors = coretools::instances::parameters().getParameterWithDefault("numNVecs" + Postfix, DefaultNumNVectors);
	coretools::instances::logfile().list("Will sample ", _numNVectors, " vectors of jumps. ('numNVecs" + Postfix + "')");

	_thinning = coretools::instances::parameters().getParameterWithDefault("thinning" + Postfix, DefaultThinning);
	coretools::instances::logfile().list("Will keep every ", _thinning, "th step. ('thinning"  + Postfix + "')");

	_burninLength = coretools::instances::parameters().getParameterWithDefault("burnin" + Postfix, DefaultBurninLength);
	coretools::instances::logfile().list("Will discard the first ", _burninLength, " steps as burnin. ('burnin"  + Postfix + "'");

	_setTotalLength();
	coretools::instances::logfile().list("The total length of each MCMC chain will thus be ", _totalLength, ".");
};

//-------------------------------------------------------------------
//TMCMCBase
//-------------------------------------------------------------------
TMCMCBase::TMCMCBase(TTree* Tree, TVariables* Vars){
	tree = Tree;
	vars = Vars;
	heat = 1.0; //default chain is not heated!

	//variables to be reset
	SigmaVectorInitialized = false;
	levyParams = NULL;
	curSigmaNvector = NULL;
	numAccepted = 0;
	acceptanceRate = 0.0;
	prog = 0;
	oldProg = 0;
	verboseProgressConclusion = true;
};

void TMCMCBase::reset(TlevyParams & LevyParams){
	levyParams = &LevyParams;
	vars->setBrownianParameters(LevyParams.rootStates(), LevyParams.brownianVariances());
	numAccepted = 0;
	prog = 0.0;
	oldProg = 0.0;

	if(!SigmaVectorInitialized) initCurSigmaNvector();
	curSigmaNvector->reset(*levyParams);
};

void TMCMCBase::initCurSigmaNvector(){
	curSigmaNvector = new TSigmaN(tree, vars);
	SigmaVectorInitialized = true;
};

void TMCMCBase::prepareMCMCStep(){
	curSigmaNvector->prepareMCMCStep();
};

void TMCMCBase::runMCMCStepForEM(const bool verbose){
	numAccepted += curSigmaNvector->performMCMCStepForEM(heat, verbose);
};

void TMCMCBase::runMCMCStepForLL(const bool verbose){
	numAccepted += curSigmaNvector->performMCMCStepForLL(heat, verbose);
};

void TMCMCBase::startProgressReport(){
	timer.start();
	coretools::instances::logfile().listFlush("Running MCMC ...");
};

void TMCMCBase::reportProgress(uint32_t iteration, uint32_t length){
	prog = 100.0 * (float) iteration/ (float) length;
	if(prog > oldProg){
		coretools::instances::logfile().overListFlush("Running MCMC ... (", prog, "%)");
		oldProg = prog;
	}
};

void TMCMCBase::concludeProgressReport(uint32_t length){
	if(verboseProgressConclusion){
		coretools::instances::logfile().overList("Running MCMC ... done (", timer.formattedTime(coretools::TimeFormat::abbreviated), ")!");
		concludeAcceptance(length);
	}
};

void TMCMCBase::concludeAcceptance(uint32_t length){
	acceptanceRate = (double) numAccepted / (double) (length * curSigmaNvector->numBranches);
	coretools::instances::logfile().conclude("Acceptance rate was ", floor(10000.0 * acceptanceRate)/100.0, "%");
};

//-------------------------------------------------------------------
//TMCMC
//-------------------------------------------------------------------
TMCMC::TMCMC(TTree* Tree, TVariables* Vars):TMCMCBase(Tree, Vars){
	curSigmaNvectorEM = NULL;
}

void TMCMC::initCurSigmaNvector(){
	curSigmaNvectorEM = new TSigmaN_EM(tree, vars);
	curSigmaNvector = curSigmaNvectorEM;
	SigmaVectorInitialized = true;
};

void TMCMC::runMCMC(const TMCMCSettings & settings){
	//other variables
	double newLambda = 0;
	startProgressReport();
	uint64_t numSampled = 0;

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){
		prepareMCMCStep();
		runMCMCStepForEM();

		//store thinned values
		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			++numSampled;
			newLambda += curSigmaNvector->numJumpsOnTree;
			curSigmaNvectorEM->SMultiplyCoef = curSigmaNvectorEM->SMultiplyCoef + 1.0;
			reportProgress(iteration, settings.totalLength());
		}
	}
	acceptanceRate = (double) numAccepted / (double) (settings.totalLength() * curSigmaNvectorEM->numBranches);

	//update lambda parameters
	newLambda = newLambda / (double) numSampled / tree->getTotalTreeLength();
	if(newLambda / levyParams->lambda() > 100.0){
		levyParams->setLambda(levyParams->lambda() * 100.0);
	} else {
		levyParams->setLambda(newLambda);
	}

	//EM step: update mu and s_0_squared for each data dimension
	curSigmaNvector->fastMultipleAddMatricesSim();
	curSigmaNvectorEM->S = curSigmaNvectorEM->S / (double) numSampled;
	vars->updateParametersEM(levyParams->rootStates(), levyParams->brownianVariances(), curSigmaNvectorEM->S);
	concludeProgressReport(settings.totalLength());
};

void TMCMC::runMCMCForConvergenceTesting(const TMCMCSettings & settings, std::ofstream & outputFile){
	//other variables
	double newLambda = 0;
	startProgressReport();
	Matrix tempMat;

	//write header
	outputFile << "Iteration\tRootState\tBrownVar\tLambda\n";
	outputFile << "Iteration\tLambda";
	for(uint16_t i = 0; i < vars->getDataDimensionality(); ++i){
		outputFile << "\tRootState" << (i+1);
	}
	for(uint16_t i = 0; i < vars->getDataDimensionality(); ++i){
		outputFile << "\tBrownVar" << (i+1);
	}
	outputFile << "\n";

	std::vector<double> rootStates(vars->getDataDimensionality());
	std::vector<double> brownVar(vars->getDataDimensionality());

	uint64_t numSampled = 0;

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){
		//curSigmaNvector->performSimpleMCMCStep();
		prepareMCMCStep();
		runMCMCStepForEM();

		//store thinned values
		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			++numSampled;
			newLambda += curSigmaNvector->numJumpsOnTree;
			curSigmaNvectorEM->SMultiplyCoef = curSigmaNvectorEM->SMultiplyCoef + 1.0;
			reportProgress(iteration, settings.totalLength());

			if(iteration > 2000){
				//Estimate current parameters and print to file
				outputFile << iteration;

				//lambda
				outputFile << "\t" << newLambda/(double) numSampled / tree->getTotalTreeLength();

				//root state and brown var
				TSigmaN_EM testtest(curSigmaNvectorEM);
				testtest.fastMultipleAddMatricesSim();
				testtest.S = testtest.S/(double) numSampled;

				vars->updateParametersEM(rootStates, brownVar, testtest.S);

				for(auto& d : rootStates){
					outputFile << "\t" << d;
				}

				for(auto& d : brownVar){
					outputFile << "\t" << d;
				}

				outputFile << "\n";
			}
		}
	}
	concludeProgressReport(settings.totalLength());
};

//-------------------------------------------------------------------
//TMCMCLL
//-------------------------------------------------------------------
TMCMCLL::TMCMCLL(TTree* Tree, TVariables* Vars, double normalizer):TMCMCBase(Tree, Vars){
	_normalizer = normalizer;
};

void TMCMCLL::runMCMC(const TMCMCSettings & settings){
	double normalizedLikelihoodSum = 0.0;
	double FisherInfo = 0.0;
	startProgressReport();
	uint64_t numSampled = 0;

	//DEBUG!!!
	//coretools::instances::randomGenerator().setSeed(100,  true);
	//length = 10;
	//DEBUG!!!

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){
		prepareMCMCStep();
		runMCMCStepForLL(false);

		//DEBUG!!!
		//std::cout << "JUMP CONFIG: " << curSigmaNvector->getJumpConfigForPrinting() << "\t" << curSigmaNvector->getLogConditionalDensity(levyParams->mu, levyParams->s_0_squared) << std::endl;
		//DEBUG!!!

		//estimate LL as average Log(Phi(N))
		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			//add to normalized likelihood sum
			normalizedLikelihoodSum += exp(curSigmaNvector->getLogConditionalDensity() - _normalizer);

			//add to Fisher info
			double n_by_lambda = curSigmaNvector->numJumpsOnTree / levyParams->lambda();
			double tmp = n_by_lambda - tree->getTotalTreeLength();
			FisherInfo += (n_by_lambda / levyParams->lambda())  - tmp*tmp;
			++numSampled;
			reportProgress(iteration, settings.totalLength());
		}
	}

	//estimate LL and Fisher
	levyParams->setModelfit(log(normalizedLikelihoodSum / (double) numSampled) + _normalizer, FisherInfo / numSampled);
	concludeProgressReport(settings.totalLength());
};

//-------------------------------------------------------------------
//TMCMCEmpiricalBayes
//-------------------------------------------------------------------
TMCMCEmpiricalBayes::TMCMCEmpiricalBayes(TTree* Tree, TVariables* Vars):TMCMCBase(Tree, Vars){}

void TMCMCEmpiricalBayes::runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	startProgressReport();

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){
		curSigmaNvector->prepareMCMCStep();
		runMCMCStepForEM();

		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress(iteration, settings.totalLength());
		}

		if(iteration >= settings.burninLength() && iteration % intermediatePosteriorThinning==0){
			writeIntermediatePosteriorTrees(outname, iteration, settings.totalLength());
		}
	}
	concludeProgressReport(settings.totalLength());
};

void TMCMCEmpiricalBayes::writeJumpPosteriors(std::string basename, bool verbose){
	//has jumps per branch
	std::string filename = basename + ".hasJumpsPerBranch.post";
	if(verbose){
		coretools::instances::logfile().list("Writing per-branch posterior probabilities to have at least one jump to '" + filename + "'.");
	}
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorTree_hasJumps() << std::endl;
	out.close();

	//number jumps per branch
	filename = basename + ".numJumpsPerBranch.post";
	if(verbose){
		coretools::instances::logfile().list("Writing per-branch posterior mean number of jumps to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorTree_numJumps() << std::endl;
	out.close();

	//number jumps per branch
	filename = basename + ".jumpScaledBranchLength.post";
	if(verbose){
		coretools::instances::logfile().list("Writing per-branch length scaled by the posterior mean number of jumps to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorTree_lengthWithJumps(levyParams->alpha()) << std::endl;
	out.close();

	//has jumps per leave
	filename = basename + ".hasJumpsPerLeave.post";
	if(verbose){
		coretools::instances::logfile().list("Writing per-leave posterior probabilities to have at least one jump to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorLeaves_hasJumps() << std::endl;
	out.close();

	//num jumps per leave
	filename = basename + ".numJumpsPerLeave.post";
	if(verbose){
		coretools::instances::logfile().list("Writing per-leave posterior mean number of jumps to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorLeaves_hasJumps() << std::endl;
	out.close();
};

void TMCMCEmpiricalBayes::writeIntermediatePosteriorTrees(std::string & outname, uint32_t iteration, uint32_t length){
	//write posterior tree
	std::string basename = outname+"_";

	int nZeros = floor(log10(length)) - floor(log10(iteration));
	for(int i=0; i<nZeros; ++i) basename += "0";
	basename += coretools::str::toString(iteration);

	writeJumpPosteriors(basename, false);
};

//-------------------------------------------------------------------
//TMCMCEmpiricalBayesNew
//-------------------------------------------------------------------

TMCMCEmpiricalBayesLongJumps::TMCMCEmpiricalBayesLongJumps(TTree* Tree, TVariables* Vars, double ProbLongJump, double ProbGeometric):TMCMCEmpiricalBayes(Tree, Vars){
	probLongJump = ProbLongJump;
	probGeometric = ProbGeometric;
	acceptanceRateLongJumps = 0.0;
};

void TMCMCEmpiricalBayesLongJumps::runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	TSigmaN* sigmaLongJump = new TSigmaN(tree, vars);
	TSigmaN* sigmaTmp;
	long numLong = 0;
	long numAcceptedLong = 0;
	startProgressReport();

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){
		if(coretools::instances::randomGenerator().getRand() < probLongJump){
			//Do a long MCMC jump: put random jumps on an empty tree
			++numLong;
			sigmaLongJump->reset(*levyParams);
			sigmaLongJump->addJumpsToRandomBranchesGeometric(probGeometric);

			//calculate hastings
			double h = sigmaLongJump->calculateHastingForGeometricTransition(curSigmaNvector, probGeometric);

			//accept or reject
			if(coretools::instances::randomGenerator().getRand() < h){
				//accept
				sigmaTmp = curSigmaNvector;
				curSigmaNvector = sigmaLongJump;
				sigmaLongJump = sigmaTmp;
				++numAcceptedLong;
			}
		} else {
			curSigmaNvector->prepareMCMCStep();
			runMCMCStepForEM();
		}

		//sampling
		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress(iteration, settings.totalLength());
		}

		if(iteration >= settings.burninLength() && iteration % intermediatePosteriorThinning==0){
			writeIntermediatePosteriorTrees(outname, iteration, settings.totalLength());
		}

	}
	delete sigmaLongJump;
	concludeProgressReport(settings.totalLength());
};

void TMCMCEmpiricalBayesLongJumps::concludeAcceptance(uint32_t length){
	TMCMCBase::concludeAcceptance(length);
	coretools::instances::logfile().conclude("acceptance rate of long jumps was ", floor(1000*acceptanceRateLongJumps)/10, "%");
};

//-------------------------------------------------------------------
//TMCMCHeated
//A heated version that does not recorded anything beyond its state
//-------------------------------------------------------------------
TMCMCEmpiricalBayesHeated::TMCMCEmpiricalBayesHeated(TTree* Tree, TVariables* Vars, int NumChains, double DeltaT):TMCMCEmpiricalBayes(Tree, Vars){
	//initialize other chains
	numChains = NumChains;
	heatedChains = new TMCMCBase*[numChains];
	heatedChains[0] = this;
	coretools::instances::logfile().list("Will use a delta T of ", DeltaT, ".");
	std::string heats = coretools::str::toString(1.0);
	for(int i=1; i<numChains; ++i){
		heatedChains[i] = new TMCMC(Tree, Vars);
		//set heat
		double heat = 1.0 / (1 + DeltaT*i);
		heatedChains[i]->setHeat(heat);
		heats += " " + coretools::str::toString(heat);
	}
	coretools::instances::logfile().conclude("Resulting in these temperature: " + heats);

	acceptanceRateSwaps = 0.0;
	numSwaps = 0;
};

void TMCMCEmpiricalBayesHeated::reset(TlevyParams & LevyParams){
	TMCMCBase::reset(LevyParams);
	//also reset heated chains
	for(int i=1; i<numChains; ++i){
		heatedChains[i]->reset(LevyParams);
	}
};

void TMCMCEmpiricalBayesHeated::initCurSigmaNvector(){
	TMCMCEmpiricalBayes::initCurSigmaNvector();
	//also init heated chains
	for(int i=1; i<numChains; ++i){
		heatedChains[i]->initCurSigmaNvector();
	}
};

bool TMCMCEmpiricalBayesHeated::swapChains(TMCMCBase* first, TMCMCBase* second){
	//calculate hastings
	double h_log;
	if(first->curSigmaNvector->numJumpsOnTree == second->curSigmaNvector->numJumpsOnTree){
		//no need for prior
		//check if at same state
		if(first->curSigmaNvector->hasSameJumps(second->curSigmaNvector)) return true;
		h_log = (first->getHeat() - second->getHeat()) * (second->curSigmaNvector->getLogConditionalDensity() - first->curSigmaNvector->getLogConditionalDensity());
	} else h_log = (first->getHeat() - second->getHeat()) * (second->getCurrentLogLikelihood() - first->getCurrentLogLikelihood());

	//swap?
	if(coretools::instances::randomGenerator().getRand() < exp(h_log)){
		//swap SigmaN
		TSigmaN* tmpSigmaN = first->curSigmaNvector;
		first->curSigmaNvector = second->curSigmaNvector;
		second->curSigmaNvector = tmpSigmaN;
		return true;
	} else return false;
};

void TMCMCEmpiricalBayesHeated::runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	numSwaps = 0;
	startProgressReport();

	for(uint32_t iteration=0; iteration<settings.totalLength(); ++iteration){

		//first store random numbers to allow for parallelizing!
		for(int i=0; i<numChains; ++i)
			heatedChains[i]->prepareMCMCStep();

		//then run MCMC step
		//#pragma omp parallel for
		for(int i=0; i<numChains; ++i)
			heatedChains[i]->runMCMCStepForEM();

		//attempt to swap states between neighboring chains (those with adjacent temperature)
		for(int thisChain=0; thisChain < (numChains-1); ++thisChain){
			if(swapChains(heatedChains[thisChain], heatedChains[thisChain+1])) ++numSwaps;
		}

		if(iteration >= settings.burninLength() && iteration % settings.thinning()==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress(iteration, settings.totalLength());
		}

		if(iteration >= settings.burninLength() && iteration % intermediatePosteriorThinning==0){
			writeIntermediatePosteriorTrees(outname, iteration, settings.totalLength());
		}

	}
	concludeProgressReport(settings.totalLength());
};

void TMCMCEmpiricalBayesHeated::concludeAcceptance(uint32_t length){
	TMCMCBase::concludeAcceptance(length);
	acceptanceRateSwaps = (double) numSwaps / (double) (length*(numChains-1));
	coretools::instances::logfile().conclude("Acceptance rate of swaps ", floor(1000*acceptanceRateSwaps)/10, "%");
};

//-------------------------------------------------------------------

