/*
 * TVariables.h
 *
 *  Created on: Jan 24, 2022
 *      Author: phaentu
 */

#ifndef CORE_TVARIABLES_H_
#define CORE_TVARIABLES_H_

#include "TTree.h"
#include "TTraitData.h"
#include "coretools/Main/TLog.h"

//------------------------------------------------
// TVariables
//------------------------------------------------
class TVariables{
private:
	void prepareVariables(const TTree & Tree);

public:
	SymmetricMatrix Tau;
	SymmetricMatrix Tau_inv;

	double Tau_det_log;
	//double pow2PiL;
	//double pow2PiL_log;
	ColumnVector* uk;
	Matrix* uk_transposed;
	Matrix ones;
	Matrix ones_transposed;

	//trait data
	int numLeaves;
	TTraitData traitData;

	//relations table
	int NrOfVectorRelations;
	double** PVectorsStore;
	int* PVectorsRel;
	int* PVectorsPop;
	int* PVectorsIdx;


	TVariables(const TTree & Tree, const std::string & Filename);
	TVariables(const TTree & Tree);
	~TVariables(){
		delete[] uk;
		delete[] uk_transposed;
		delete[] PVectorsPop;
		delete[] PVectorsIdx;
		delete[] PVectorsRel;
		delete[] PVectorsStore;
	};

	size_t getDataDimensionality() const { return traitData.dimensionality(); };
	void setBrownianParameters(const std::vector<double> & RootStates, const std::vector<double> & s_0_squared);
	double getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log);
	void updateParametersEM(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & S) const;
	double hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx);
	double computeNullModelMLE(std::vector<double> & RootStates, std::vector<double> & s_0_squared);
	void performFTest(std::vector<int> & branchesWithJumps);
};



#endif /* CORE_TVARIABLES_H_ */
