/*
 * TTraitData.cpp
 *
 *  Created on: Sep 26, 2021
 *      Author: phaentu
 */

#include "TTraitData.h"

//-------------------------------------------------------------
//TTraitDataOneDimension
//-------------------------------------------------------------
TTraitDataOneDimension::TTraitDataOneDimension(const uint16_t & NumLeaves){
	x.ReSize(NumLeaves);
	x_orig.ReSize(NumLeaves);
	x_transposed.ReSize(NumLeaves,1);
	_ones.ReSize(NumLeaves,1); _ones = 1;
	_ones_transposed = _ones.t();
	_matrixSize_s = NumLeaves * (NumLeaves + 1) / 2;
	_s_0_squared = 1.0;
	_log_s_0_squared = 0.0;
};

TTraitDataOneDimension::~TTraitDataOneDimension(){};

void TTraitDataOneDimension::setTrait(const uint16_t & LeaveIndex, const double & Trait){
	x_orig(LeaveIndex+1) = Trait;
};

void TTraitDataOneDimension::setBrownianParameters(double RootState, double s_0_squared){
	x = x_orig - RootState;
	x_transposed = x.t();
	_s_0_squared = s_0_squared;
	_log_s_0_squared = log(_s_0_squared);
};

double TTraitDataOneDimension::_MulSym_xtMx(double* M) const{
	//compute matrix multiplication x.t() * M * x of a symmetric matrix M
	double* tmpStorage = new double[x.size()];
	int start, end, resCol;
	for(int row = 0; row < x.size(); ++row){
		end = (row*(row+3)) >> 1;
		tmpStorage[row] = x.element(row) * M[end];
	}
	end = 0;
	for(int row = 1; row < x.size(); ++row){
		start = end + 1;
		end = (row*(row+3)) >> 1;
		resCol=0;
		for(; start<end; ++start, ++resCol){
			tmpStorage[resCol] += x.element(row) * M[start];
			tmpStorage[row] += x.element(resCol) * M[start];
		}
	}

	double res = 0.0;
	for(int row = 0; row < x.size(); ++row){
		res += tmpStorage[row] * x.element(row);
	}
	delete[] tmpStorage;
	return res;
};

double TTraitDataOneDimension::SparseMulSim_xtMv(double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx){
	double ResSum = 0;
	int i,j,l,k;
	if (LeafIdx > -1){
		l = (LeafIdx*(LeafIdx+1)) >> 1;
		for(j=0;j<LeafIdx;j++){
			ResSum += AlphaInvMatrixData[l+j] * x.element(j);
		}

		l = (LeafIdx*(LeafIdx+3)) >> 1;
		for(j=LeafIdx;j<x_orig.size();j++){
			ResSum += AlphaInvMatrixData[l] * x.element(j);
			l += j+1;
		}
		return ResSum;
	}

	for(i=0;i<Pop;i++){
		k = PVectorsRel[StartIdx+i];
		l = (k*(k+1)) >> 1;
		for(j=0;j<k;j++){
			ResSum += AlphaInvMatrixData[l+j] * x.element(j);
		}
		l = (k*(k+3)) >> 1;
		for(j=k;j<x_orig.size();j++){
			ResSum += AlphaInvMatrixData[l] * x.element(j);
			l += j+1;
		}
	}
	return ResSum;
};

double TTraitDataOneDimension::getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log) const{
	//first compute S_inv
	static double* _Sinv = new double[_matrixSize_s];
	for(uint32_t i=0; i<_matrixSize_s; ++i){
		_Sinv[i] = AlphaInvMatrixData[i] / _s_0_squared;
	}

	//compute x.t() * S_inv * x
	double cond_dens = _MulSym_xtMx(_Sinv);

	//compute density
	double S_det_log = (double) x.size() * _log_s_0_squared + TNAlpha_det_log;
	return -0.50 * ( x.size() * 1.837877 + S_det_log + cond_dens); //log(2*pi) = 1.837877
};

void TTraitDataOneDimension::updateParametersEM(double & RootState, double & s_0_squared, const SymmetricMatrix & S) const{
	//update root state
	RootState = (_ones_transposed * S * x_orig).AsScalar() / (_ones_transposed * S * _ones).AsScalar();

	//update s_0_squared
	Matrix tempMat = (x_orig - RootState * _ones);
	s_0_squared = (tempMat.t() * S * tempMat).AsScalar() / (double) _ones.size();
};

double TTraitDataOneDimension::hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx){
	double TempPar = SparseMulSim_xtMv(AlphaInvMatrixData, PVectorsRel, Pop, LeafIdx, StartIdx);
	return alpha_times_delta_n_k / (2 * r_k * _s_0_squared) * TempPar * TempPar;
};

double TTraitDataOneDimension::estimateNullModelMLE(double & RootState, double & s_0_squared, const SymmetricMatrix & Tau_inv, const double & Tau_det_log){
	//precompute things
	double* tau_inv_store = Tau_inv.Store();
	double* x_orig_store = x_orig.Store();
	double* res = new double[x.size()];
	double* res_x = new double[x.size()];
	int start, end, resCol;
	for(int row = 0; row < x.size(); ++row){
		end = (row*(row+3)) >> 1;
		res[row] = tau_inv_store[end];
		res_x[row] = res[row] * x_orig_store[row];
	}
	end = 0;
	for(int row = 1; row < x.size(); ++row){
		start = end + 1;
		end = (row*(row+3)) >> 1;
		for(resCol=0; start<end; ++start, ++resCol){
			res[resCol] += tau_inv_store[start];
			res[row] += tau_inv_store[start];
			res_x[resCol] += x_orig_store[row] * tau_inv_store[start];
			res_x[row] += x_orig_store[resCol] * tau_inv_store[start];
		}
	}
	double ones_transposed_Tau_inv_x = 0.0;
	double ones_transposed_Tau_inv_ones = 0.0;
	double x_transposed_Tau_inv_x = 0.0;
	for(int row = 0; row < x.size(); ++row){
		ones_transposed_Tau_inv_ones += res[row];
		ones_transposed_Tau_inv_x += res[row] * x_orig_store[row];
		x_transposed_Tau_inv_x += res_x[row] * x_orig_store[row];
	}
	delete[] res;
	delete[] res_x;

	//compute MLE
	RootState = ones_transposed_Tau_inv_x / ones_transposed_Tau_inv_ones;
	s_0_squared = 1.0/(double) (x.size()-1.0) * (x_transposed_Tau_inv_x - ones_transposed_Tau_inv_x * ones_transposed_Tau_inv_x / ones_transposed_Tau_inv_ones);

	//update parameters for LL calculation
	setBrownianParameters(RootState, s_0_squared);

	//compute Likelihood
	/*
	int length = x.size()*(x.size()+1) >> 1;
	double* S_inv_new = new double[length];

	for(int i=0; i<length; ++i) S_inv_new[i] = tau_inv_store[i] / s_0_squared;
	double S_det = x.size()*log(s_0_squared) + Tau_det_log;

	//compute x.t() * S_inv * x
	double nullModelLikelihood = _MulSym_xtMx(S_inv_new);
	nullModelLikelihood = - 0.5 * (x.size() * 1.837877 + S_det) - 0.50 * nullModelLikelihood; //log(2*pi) = 1.837877
	delete[] S_inv_new;

	return nullModelLikelihood;
	*/

	return getLogConditionalDensity(tau_inv_store, Tau_det_log);
};

void TTraitDataOneDimension::performFTest(const int & K, const Matrix & X, const SymmetricMatrix & Tau, const SymmetricMatrix & Tau_inv, const double & Tau_det_log){
	//estimate beta hat
	ColumnVector betaHat = (X.t() * Tau_inv * X).i() * X.t() * Tau_inv * x_orig;

	//estimate s_square
	setBrownianParameters(0.0, 1.0); //TODO: check if that makes sense!
	double s2 = _MulSym_xtMx(Tau_inv.Store());

	s2 = s2 - (x_orig.t() * Tau_inv * X * betaHat).AsScalar();
	s2 = s2 * 1.0/(x_orig.size() - K - 1.0);

	//compute F
	Matrix R(K, K+1);
	R = 0.0;
	for(int i=1; i<(K+1); ++i) R(i, i+1) = 1;

	double F = 1/(K*s2) * (betaHat.t() * R.t() * (R * (X.t() * Tau_inv * X).i() * R.t()).i() * R * betaHat).AsScalar();

	//estimate model fit
	ColumnVector epsilon = x_orig - X * betaHat;
	double xxx = (epsilon.t() * (s2 * Tau).i()  * epsilon).AsScalar();

	//output
	coretools::instances::logfile().list("F-Stat = ", F);
	coretools::instances::logfile().list("1 - pf(" ,F, ", df1=", K, ", df2=", x_orig.size() - K - 1, ")");
	coretools::instances::logfile().list("Chi^2 = ", xxx);
	coretools::instances::logfile().list("1 - pchisq(", xxx, ", df=", x_orig.size() - K - 1, ")");
};

//-------------------------------------------------------------
//TTraitData
//-------------------------------------------------------------
void TTraitData::readTraitFile(const std::string & Filename, const TTree & tree){
	coretools::instances::logfile().listFlush("Reading trait values from '" + Filename + "' ...");
	coretools::TInputFile file(Filename, coretools::TFile_Filetype::fixed);

	//adjust dimensionality
	uint16_t _numLeaves = tree.getNumLeaves();
	uint16_t numDimensions = file.numCols() - 1;
	data.clear();
	data.reserve(numDimensions);
	for(uint16_t i = 0; i < numDimensions; ++i){
		data.emplace_back(_numLeaves);
	}

	//store for which leaves traits were read
	std::vector<bool> traitRead(_numLeaves);

	//read file
	std::vector<std::string> vec;
	while(file.read(vec)){
		//match species name to tree label
		uint16_t num = tree.getLeaveNumberFromName(vec[0]);
		traitRead[num] = true;

		//store trait values
		for(uint16_t i = 0; i < numDimensions; ++i){
			data[i].setTrait(num, coretools::str::fromStringCheck<double>(vec[i+1]));
		}
	}

	//check if all leaves have data
	for(uint16_t i = 0; i < _numLeaves; ++i){
		if(!traitRead[i]){
			throw "Missing trait values for leave '" + tree.getLeaveName(i) + "'!";
		}
	}

	//compute first X with root state 0 and s_0_squared 1.0
	for(uint16_t i=0; i<data.size(); ++i){
		data[i].setBrownianParameters(0.0, 1.0);
	}

	//report
	coretools::instances::logfile().done();
	coretools::instances::logfile().conclude("Read traits with ", numDimensions, " dimensions.");
};

void TTraitData::setBrownianParameters(const std::vector<double> & RootStates, const std::vector<double> & s_0_squared){
	for(uint16_t i=0; i<data.size(); ++i){
		data[i].setBrownianParameters(RootStates[i], s_0_squared[i]);
	}
};

double TTraitData::getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log){
	double cond_dens = 0.0;
	for(uint16_t i = 0; i < data.size(); ++i){
		cond_dens += data[i].getLogConditionalDensity(AlphaInvMatrixData, TNAlpha_det_log);
	}
	return cond_dens;
};

void TTraitData::updateParametersEM(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & S) const{
	for(uint16_t i = 0; i < data.size(); ++i){
		data[i].updateParametersEM(RootStates[i], s_0_squared[i], S);
	}
};

double TTraitData::hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx){
	double h = 0.0;
	for(uint16_t i = 0; i < data.size(); ++i){
		h += data[i].hastingsTerm(alpha_times_delta_n_k, r_k, AlphaInvMatrixData, PVectorsRel, Pop, LeafIdx, StartIdx);
	}
	return h;
};

double TTraitData::estimateNullModelMLE(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & Tau_inv, const double & Tau_det_log){
	RootStates.resize(data.size());
	s_0_squared.resize(data.size());
	double nullModelLL = 0.0;
	for(uint16_t i = 0; i < data.size(); ++i){
		nullModelLL += data[i].estimateNullModelMLE(RootStates[i], s_0_squared[i], Tau_inv, Tau_det_log);
	}
	return nullModelLL;
};

void TTraitData::performFTest(const int & K, const Matrix & X, const SymmetricMatrix & Tau, const SymmetricMatrix & Tau_inv, const double & Tau_det_log){
	for(uint16_t i = 0; i < data.size(); ++i){
		coretools::instances::logfile().startIndent("Data dimension ", i+1, ":");
		data[i].performFTest(K, X, Tau, Tau_inv, Tau_det_log);
		coretools::instances::logfile().endIndent();
	}
};


