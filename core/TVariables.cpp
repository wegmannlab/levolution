/*
 * TVariables.cpp
 *
 *  Created on: Jan 24, 2022
 *      Author: phaentu
 */

#include "TVariables.h"

//-------------------------------------------------------------
//TVariables
//-------------------------------------------------------------
TVariables::TVariables(const TTree & Tree, const std::string & Filename){
	numLeaves = Tree.getNumLeaves();

	//read file with trait values
	traitData.readTraitFile(Filename, Tree);

	//prepare variables
	prepareVariables(Tree);
};

TVariables::TVariables(const TTree & Tree){
	//Read Traits

	//prepare variables
	prepareVariables(Tree);
};

void TVariables::prepareVariables(const TTree & Tree){
	coretools::instances::logfile().startIndent("Preparing variables:");

	//-------------------------------------------------
	//Compute Tau
	//each entry Tau_ij is the length of the common branches as measured from the root.
	coretools::instances::logfile().listFlush("Computing Tau matrix ... (0%)");

	//Initialization part
	clock_t starttime=clock();
	int k = 0, j = 0;
	int NodeNr = Tree.getNumNodes();

	TBaseNode* ParentOfNode;

	for(k=0;k<NodeNr;k++){
		Tree.nodes[k]->NodeIdx = k;
		Tree.nodes[k]->LeafIdx = -1;
		ParentOfNode = Tree.nodes[k]->getParent();
		if (ParentOfNode == NULL) Tree.nodes[k]->ParentNodeIdx = -1;
		else  Tree.nodes[k]->ParentNodeIdx = ParentOfNode->NodeIdx;
	}

	int Counter = 0;
	std::map<std::string, TBaseNode*>::const_iterator it, ite = Tree.leaves.end();
	for(it = Tree.leaves.begin();it != ite; ++it){
		it->second->LeafIdx = Counter;
		Counter++;
	}

	//SymmetricMatrix Tau = SymmetricMatrix(numLeaves);
	Tau.ReSize(numLeaves);
	double* Tau_Ext = new double [NodeNr*NodeNr];
	double dist;
	int ParentIdx;
	int LeavesIdx;
	int LeavesIdx2;

	int Leaves = 0;
	int LeafNodeCode;
	int* NodeIdxVector = new int [numLeaves];

	long toDo=(numLeaves * numLeaves + numLeaves) / 2;
	long done=0;
	int oldp=0;

	//Main calculation part
	for(k=0; k<NodeNr; k++){
		const TBaseNode node = Tree.getNode(k);
		ParentIdx = node.getParentNodeIdx();
		dist = node.getDistance();
		if (ParentIdx >= 0) dist += Tau_Ext[ParentIdx*NodeNr+ParentIdx];
		Tau_Ext[k*NodeNr+k] = dist;

		for(j=0;j<=ParentIdx;j++) Tau_Ext[j*NodeNr+k] = Tau_Ext[j*NodeNr+ParentIdx];
		for(j=ParentIdx+1;j<=k-1;j++) Tau_Ext[j*NodeNr+k] = Tau_Ext[ParentIdx*NodeNr+j];

		LeavesIdx = node.getLeafeNodeIdx();
		if (LeavesIdx > -1){
			NodeIdxVector[Leaves] = k;
			Leaves++;

			for(j=0;j<Leaves;j++){
				LeafNodeCode = NodeIdxVector[j];
				LeavesIdx2 = Tree.getNode(LeafNodeCode).getLeafeNodeIdx();
				Tau.element(LeavesIdx,LeavesIdx2) = Tau_Ext[LeafNodeCode*NodeNr+k];
				done++;
			}
		}
		//report progress
		int p=100.0 * (float) done/ (float) toDo;
		if(p>oldp){
			coretools::instances::logfile().overListFlush("Computing Tau matrix ... (", p,  "%)");
			oldp=p;
		}
	}

	float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC;
	coretools::instances::logfile().overList("Computing Tau matrix ... done (in ", runtime, "s)!");

	//----------------------------------------
	//Compute u
	coretools::instances::logfile().listFlush("Computing vector u_k ... (0%)");
	starttime=clock();

	oldp = 0;
	done = 0;

	uk=new ColumnVector[NodeNr];
	uk_transposed=new Matrix[NodeNr];
	double* ColPtr;
	double RelVal;
	double SelfVal;

	for(int k=0; k<NodeNr; k++){
		uk[k].resize(numLeaves);
		ColPtr = uk[k].Store();

		memset(ColPtr,0,sizeof(double)*numLeaves);

		if(Tree.getNode(k).getLeafeNodeIdx() > -1){
			j = Tree.getNode(k).getLeafeNodeIdx();
			ColPtr[j] = 1;
		} else {
			SelfVal = Tau_Ext[k*NodeNr+k];
			for(int i=0; i<=k; i++) {
				RelVal = Tau_Ext[i*NodeNr+k];
				if (SelfVal < (RelVal + 0.0000000000001)) {
					j = Tree.getNode(i).getLeafeNodeIdx();
					if (j > -1) ColPtr[j] = 1;
				}
			}
			for(int i=k+1; i<NodeNr; i++){
				RelVal = Tau_Ext[k*NodeNr+i];
				if (SelfVal < (RelVal + 0.0000000000001)){
					j = Tree.getNode(i).getLeafeNodeIdx();
					if (j > -1) ColPtr[j] = 1;
				}
			}
		}

		uk_transposed[k]=uk[k].t();
		//report progress
		int p=100.0 * (float) k/ (float) NodeNr;
		if(p>oldp){
			coretools::instances::logfile().overListFlush("Computing vector u_k ... (", p, "%)");
			oldp=p;
		}
	}
	runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC;
	coretools::instances::logfile().overList("Computing vector u_k ... done (in ", runtime, "s)!");

	delete[] Tau_Ext;
	delete[] NodeIdxVector;
	//End of modification

	//------------------------------------------
	//compute the inverse of Tau
	coretools::instances::logfile().listFlushTime("Computing inverse and determinant of Tau ...");

	Tau_inv=Tau.i();
	Tau_det_log=Tau.LogDeterminant().LogValue();
	coretools::instances::logfile().doneTime();

	//------------------------------------------
	//generate relation table
	PVectorsStore = new double* [Tree.getNumNodes()];
	for(unsigned int i=0;i<Tree.getNumNodes();i++) PVectorsStore[i] = uk_transposed[i].Store();

	int Population;
	NrOfVectorRelations = 0;
	PVectorsPop = new int [Tree.getNumNodes()];
	PVectorsIdx = new int [Tree.getNumNodes()];

	for(unsigned int i=0;i<Tree.getNumNodes();i++){
		Population = 0;
		for(unsigned int j=0;j<Tree.getNumLeaves();j++){
			if (PVectorsStore[i][j] != 0) Population++;
		}
		PVectorsPop[i] = Population;
		PVectorsIdx[i] = NrOfVectorRelations;
		NrOfVectorRelations += Population;
	}

	PVectorsRel = new int [NrOfVectorRelations];
	int RelIdx = 0;
	for(unsigned int i=0;i<Tree.getNumNodes();i++){
		for(unsigned int j=0;j<Tree.getNumLeaves();j++){
			if (PVectorsStore[i][j] != 0){
				PVectorsRel[RelIdx] = j;
				RelIdx++;
			}
		}
	}

	//----------------------------------------
	//precompute things
	//prepare matrices of ones
	ones.ReSize(Tree.getNumLeaves(),1); ones = 1;
	ones_transposed = ones.t();
	coretools::instances::logfile().endIndent();
};

double TVariables::getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log){
	return traitData.getLogConditionalDensity(AlphaInvMatrixData, TNAlpha_det_log);
};

void TVariables::setBrownianParameters(const std::vector<double> & RootStates, const std::vector<double> & s_0_squared){
	traitData.setBrownianParameters(RootStates, s_0_squared);
};

void TVariables::updateParametersEM(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & S) const{
	traitData.updateParametersEM(RootStates, s_0_squared, S);
};

double TVariables::hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx){
	return traitData.hastingsTerm(alpha_times_delta_n_k, r_k, AlphaInvMatrixData, PVectorsRel, Pop, LeafIdx, StartIdx);
};

double TVariables::computeNullModelMLE(std::vector<double> & RootStates, std::vector<double> & s_0_squared){
	return traitData.estimateNullModelMLE(RootStates, s_0_squared, Tau_inv, Tau_det_log);
};

void TVariables::performFTest(std::vector<int> & branchesWithJumps){
	//detect branches with jumps
	int K = branchesWithJumps.size();

	//construct X
	Matrix X(numLeaves, branchesWithJumps.size() + 1);
	X=1.0;
	int col=2;
	for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it, ++col){
		X.Column(col) = uk[*it];
	}

	//run test
	traitData.performFTest(branchesWithJumps.size(), X, Tau, Tau_inv, Tau_det_log);
};

