/*
 * TConvergenceTest.h
 *
 *  Created on: Feb 17, 2012
 *      Author: wegmannd
 */

#ifndef TCONVERGENCETEST_H_
#define TCONVERGENCETEST_H_

#include "TSigmaN.h"
#include "coretools/Files/TOutputFile.h"

class TConvergenceTest{
private:
	double* values;
	int numValues;
	int nextVal;
	bool full;
	double meanVal;
	double Sxx;
	double Sxy;
	double sumSquared;
	double T, N;

	void computeT(const double & val){
		//adjust summaries
		if(full){
			meanVal -= values[nextVal]/(double) numValues;
			Sxy += 0.5 * ((double)numValues + 1.0 - 2.0*((double)nextVal+1.0))*values[nextVal];
			sumSquared -= values[nextVal] * values[nextVal];
		}
		meanVal+=val/(double)numValues;
		Sxy-=0.5 * ((double)numValues + 1.0 - 2.0*((double)nextVal+1))*val;
		sumSquared+=val * val;

		//store value
		 values[nextVal]=val;
		 ++nextVal;
		 if(nextVal==numValues){
			 nextVal=0;
			 full=true;
		 }

		 //check convergence -> return false if array is not full
		 if(full){
			 //perform test
			 double betaHat=Sxy/Sxx;
			 double sigmaSquared=1.0/((double)numValues-2.0) * (sumSquared - (double) numValues*meanVal*meanVal - betaHat * Sxy);
			 double se = sqrt(sigmaSquared / Sxx);
			 T=betaHat / se;
			 if(T<0.0) T=-T;
		 }
	};

	void computePablosN(){
		//calculate the fraction of switches in the slope
		//get sign of last slope
		if(full){
			int index = nextVal - 1;
			if(index < 0) index = numValues - 1;
			int previousIndex = nextVal - 2;
			if(previousIndex < 0) previousIndex = numValues - 1;
			bool sign, lastSign;
			if(values[index] > values[previousIndex]) lastSign = true;
			else lastSign = false;
			int numSignChanges = 0;

			//now go through all other slopes sequentially
			for(int i=0; i<(numValues-2); ++i){
				//get index
				index = previousIndex;
				previousIndex = index - 1;
				if(previousIndex < 0) previousIndex = numValues - 1;

				//calc slope
				if(values[index] > values[previousIndex]) sign = true;
				else sign = false;
				if(sign != lastSign) ++numSignChanges;
				lastSign = sign;
			}

			//return fraction
			N = (double) numSignChanges / (double) (numValues - 2);
		}
	};

public:
	TConvergenceTest(){
		numValues = 0;
		values = NULL;
		nextVal = 0;
		full = false;
		meanVal=0;
		Sxx = 0.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	~TConvergenceTest(){
		if(numValues>0) delete[] values;
	};

	TConvergenceTest(int NumValues){
		numValues=NumValues;
		values = new double[numValues];
		nextVal = 0;
		full = false;
		meanVal = 0.0;
		Sxx = numValues * (numValues * numValues - 1.0) / 12.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	void initialize(int NumValues){
		numValues=NumValues;
		values = new double[numValues];
		Sxx = numValues * (numValues * numValues - 1.0) / 12.0;
	};

	void clear(){
		nextVal = 0;
		full = false;
		meanVal = 0.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	void resetToLast(int num){
		//check if there are enough values
		if(num < numValues){ //else keep all
			if(num < nextVal || full){ //else also keep all
				//remove num values -> recompute things for last num values
				//where to start
				int index = nextVal - num;
				if(index < 0) index = numValues + index;

				//copy values into new container
				double* tmp = new double[num];
				for(int j=0; j<num; ++j){
					tmp[j] = values[index];
					++index;
					if(index == numValues) index = 0;
				}

				//now add them again
				nextVal = 0;
				full = false;
				meanVal = 0.0;
				Sxy = 0.0;
				T = -999999.0;
				N = -1.0;
				for(int j=0; j<num; ++j){
					computeT(tmp[j]);
				}
			}

		}
	};

	void resetToHalf(){
		resetToLast(numValues / 2);
	};

	void add(const double & val){
		computeT(val);
		computePablosN();
	};

	double getAbsT() const{
		if(full) return T;
		else return -999999.0;
	};

	double getN() const{
		if(full) return N;
		else return 0.0;
	};

	bool isFull() const{
		return full;
	};
};

class TConvergenceTestVector{
private:
	uint16_t _dataDimensionality;

public:
	std::vector<TConvergenceTest> tests;
	double maxT, minN;
	double trendCheckMaxT, trendCheckMinN;
	bool full;
	int numValues;


	TConvergenceTestVector(const uint16_t & DataDimensionality, const int & NumValues, const double & MaxT, const double & MinN){
		_dataDimensionality = DataDimensionality;
		numValues = NumValues;
		tests.resize(1 + 2*_dataDimensionality); //one for lambda, and one for mu and s_0 per dimension
		for(auto& t : tests){
			t.initialize(NumValues);
		}
		maxT = -1.0;
		minN = 0.0;
		full = false;
		trendCheckMaxT = MaxT;
		trendCheckMinN = MinN;
	};

	void add(const TlevyParams & params){
		//add lambda
		tests[0].add(params.lambda());
		size_t index = 1;
		for(auto& d : params.rootStates()){
			tests[index].add(d);
			++index;
		}
		for(auto& d : params.brownianVariances()){
			tests[index].add(d);
			++index;
		}

		//update maxT and minN across tests
		if(tests[0].isFull()){
			full = true;
			maxT = tests[0].getAbsT();
			minN = tests[0].getN();

			for(size_t t = 1; t < tests.size(); ++t){
				if(tests[t].getAbsT() > maxT){
					maxT = tests[t].getAbsT();
				}
				if(tests[t].getN() < minN){
					minN = tests[t].getN();
				}
			}
		}
	};

	void addParameterNames(std::vector<std::string> & Names) const{
		//T
		Names.push_back("T_lambda");
		coretools::str::addExpandedIndex(Names, std::string("T_") + "rootState_", _dataDimensionality);
		coretools::str::addExpandedIndex(Names, std::string("T_") + "brownianVariance_", _dataDimensionality);
		Names.push_back("max_T");

		//N
		Names.push_back("N_lambda");
		coretools::str::addExpandedIndex(Names, std::string("N_") + "rootState_", _dataDimensionality);
		coretools::str::addExpandedIndex(Names, std::string("N_") + "brownianVariance_", _dataDimensionality);
		Names.push_back("min_N");
	};

	void write(coretools::TOutputFile & out) const {
		if(full){
			//T
			for(auto& t : tests){
				out.write(t.getAbsT());
			}
			out.write(maxT);

			//N
			for(auto& t : tests){
				out.write(t.getN());
			}
			out.write(minN);
		} else {
			for(size_t t = 0; t < tests.size() * 2 + 2; ++t){
				out.write("-");
			}
		}
	};


	void printToLog() const {
		if(full){
			coretools::instances::logfile().flushNumberFixedWidth(maxT, 2, 7);
			coretools::instances::logfile().flushNumberFixedWidth(minN, 2, 7);
		} else {
			coretools::instances::logfile().flushFixedWidth('-', 7);
			coretools::instances::logfile().flushFixedWidth('-', 7);
		}
	};

	bool checkTrend() const {
		if(!full) return false;
		if(maxT < trendCheckMaxT && minN > trendCheckMinN) return true;
		return false;
	};

	void clear(){
		for(auto& t : tests){
			t.clear();
		}
		full = false;
	};

	void resetTo(int num){
		for(auto& t : tests){
			t.resetToLast(num);
		}
		full = false;
	};

	void resetToHalf(){
		for(auto& t : tests){
			t.resetToHalf();
		}
		full = false;
	};
};


#endif /* TCONVERGENCETEST_H_ */
