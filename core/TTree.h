/*
 * TTree.h
 *
 *  Created on: Feb 28, 2009
 *      Author: wegmannd
 */

#ifndef TTREE_H_
#define TTREE_H_

#include "coretools/Strings/stringFunctions.h"
#include <vector>
#include <map>
#include "coretools/Main/TRandomGenerator.h"
#include <algorithm>
#include <sstream>
#include <iostream>


//---------------------------------------------------------------
class TBaseNode {
protected:
	double distance; //is the distance to the parent...

public:
	int jumps;
	double value;
	double trait;
	std::vector<TBaseNode*> children;
	std::string name;
	int numDescendantLeaves;
	int NodeIdx;
	int LeafIdx;
	int ParentNodeIdx;

	TBaseNode(){
		distance=0;
		jumps = 0;
		value=0.0;
		trait=0;
		numDescendantLeaves=0;
		NodeIdx=0;
		LeafIdx=0;
		ParentNodeIdx=0;
	};
	virtual ~TBaseNode(){};

	double getDistance() const { return distance; };
	void setDistance(double Distance){ distance = Distance; };
	void scale(double & scale){ distance *= scale; };
	void setValue(double & Val){value = Val;}
	void setJumps(int & Jumps){jumps = Jumps;}
	void sampleJumps(const double lambda);
	void simulateJumpEvolution(const double poissonVar);
	void simulateTraitEvolution(const double brownianVar);
	void addChild(TBaseNode* child){ children.push_back(child); };
	int getNumChildren(){return children.size();};
	double getValue(){return value;};
	//double getPoissonProb(const double & rate, const int & n);
	std::string getNewick();
	std::string getNewickValues();
	std::string getNewickJumps();
	void setTrait(double Trait){ trait=Trait;};
	double getTrait(){ return trait;};
	virtual TBaseNode* getParent(){return NULL;};
	int getParentNodeIdx() const { return ParentNodeIdx; };
	int getLeafeNodeIdx() const { return LeafIdx; };
	void propagateBrownianTrait(const double Trait, const double brownianVar);
	void propagateLevyTrait(const double Trait, const double brownianVar, const double poissonVar, const double lambda);
	void propagateLevyTraitExisitingJumps(const double Trait, const double brownianVar, const double poissonVar);
	void propagateLevyTraitFixedNumJumps(const double Trait, const double brownianVar, const double poissonVar, double & cumulLength, std::vector<double> & waitingTimes, std::vector<double>::iterator & waitingTimesIt);
	virtual void saveAllParents(std::vector<TBaseNode*>){};
	virtual bool isParent(TBaseNode*){return false;};
	virtual double getDistanceToRoot(){ return 0.0;};
	virtual double getCumulativeValueToRoot(){ return 0;};
	virtual TBaseNode* getCommonParent(TBaseNode*){return NULL;};
	virtual double getCommonBranchLength(TBaseNode*){return -1;};
	int getNumDescendantLeaves(){return numDescendantLeaves;};
	int computeNumDescendantLeaves();
	virtual double getCumulativeValueCommonBranches(TBaseNode*){return -1;};
	void setName(std::string Name){name=Name;};
	std::string getName(){return name;};
};

class TNode:public TBaseNode{
private:
	TBaseNode* parent;
	std::vector<TBaseNode*> allParents;
public:
	TNode(TBaseNode* Parent, double Distance, std::string Name=""){
		distance=Distance;
		parent=Parent;
		name=Name;
	};

	TBaseNode* getParent(){return parent;};
	void saveAllParents(std::vector<TBaseNode*> p);
	bool isParent(TBaseNode* other);
	TBaseNode* getCommonParent(TBaseNode* other);
	double getDistanceToRoot(){return distance+parent->getDistanceToRoot();}
	double getCumulativeValueToRoot(){return value+parent->getCumulativeValueToRoot();};
	double getCommonBranchLength(TBaseNode* other);
	double getCumulativeValueCommonBranches(TBaseNode* other);
};

class TRoot:public TBaseNode{
public:
	TRoot(){};
	TRoot(double Distance){distance=Distance;name="Root";}
	~TRoot(){};
	void saveAllParents();
	TBaseNode* getParent(){return NULL;};
	bool isParent(TBaseNode*){ return false; };
	double getDistanceToRoot(){ return distance; }
	double getCumulativeValueToRoot(){ return value; }
	TBaseNode* getCommonParent(TBaseNode*){ return this; };
	double getCommonBranchLength(TBaseNode*){ return distance; };
	double getCumulativeValueCommonBranches(TBaseNode*){ return 0.0; };
};
//---------------------------------------------------------------
//---------------------------------------------------------------
class TTree {
private:
	void makeChildNodes(std::string newickSnipplet, TBaseNode* parent=NULL);
	std::string newick;
	double probability;
	double traitMean, traitVar;
	double totLength;
	bool traitMeanComputed, traitVarComputed, numDescendantLeavesComputed;
	void initialize(const std::string & Newick, const double & Probability);

	void _computeTotalTreeLength();

public:
	std::vector<TBaseNode*> nodes;
	std::map<std::string, TBaseNode*> leaves;
	TRoot* root;
	bool distancesKnown;
	bool allParentsSaved;

	TTree(){
		probability = 0.0;
		traitMean = 0.0;
		traitVar = 0.0;
		totLength = 0.0;
		traitMeanComputed=false;
		traitVarComputed=false;
		numDescendantLeavesComputed=false;
		distancesKnown=true;
		allParentsSaved=false;
		root = NULL;
	};
	TTree(std::string Newick);
	TTree(std::string Newick, double Probability);
	TTree(int & numSpecies, double birthrate, double deathrate, double age);
	TTree(const TTree & other);
	virtual ~TTree(){
		for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i) delete (*i);
	};
	double getProbability(){ return probability; }
	uint16_t getNumLeaves() const;
	uint16_t getNumNodes() const;
	const TBaseNode getNode(const int & Idx) const { return *nodes[Idx]; };
	TBaseNode* getLeaveFromName(std::string name);
	uint16_t getLeaveNumberFromName(const std::string & name) const;
	TBaseNode* getParentOfNodeFromName(std::string name);
	double getBranchLengthFromName(std::string name);
	double getBranchLength(int node){return nodes[node]->getDistance();};
	std::string getNewick();
	std::string getNewickValues();
	std::string getNewickJumps();

	//functions to simlate
	void evoloveTrait(const double rootState, const double brownianVariance);
	void evoloveTraitLevy(const double rootState, const double brownianVariance, const double poissonVariance, double const lambda);
	void evolveTraitLevyExisitingJumps(const double rootState, const double brownianVariance, const double poissonVariance);
	void evolveTraitLevyFixedNumJumps(double rootState, double brownianVariance, double poissonVariance, int numJumps);

	double getTraitValue(int node);
	double getTraitMean();
	double getTraitVariance();
	double getCommonBranchLength(int i, int j);
	void sampleValuePoisson(double & lambda);
	void setValue(double Val);
	void setJumps(int jumps);
	void setRootDistanceToZero(){root->setDistance(0.0);};
	double getCumulativeValueCommonBranches(int i, int j);
	double getValueOneBranch(int num);
	double getCumulativeValue();
	int getNumJumps();
	void setValueOfBranch(int branch, double Val);
	void addValueToBranch(int branch, double Val);
	void setNumJumpsOnBranch(int branch, int jumps);
	bool isSubordinate(int node, int leave);
	int getNumDescendantLeaves(int node);
	double getTotalTreeLength() const;
	void scaleTree(double length);
	std::string getLeaveName(int leave) const;
	double getLeaveTrait(int leave) const;
	std::vector<double>  getLeaveTraits();
	void printLeaveNamesToSTDout(std::string delim="\t");
	void printLeaveTraitValuesToSTDout(std::string delim="\t");
	void printLeaveNameTraitTable(std::string delim="\t");
	void printLeaveNameTraitTable(std::ofstream & out, std::string delim="\t");
	void printLeaveNameTraitTableWithPrefix(std::string prefix, std::string delim);
	int getRandomNodeWithBranchlengthWeights();
	int getRandomNode();
	bool hasPolytomy();
};

#endif /* TTREE_H_ */
