/*
 * TTraitData.h
 *
 *  Created on: Sep 26, 2021
 *      Author: phaentu
 */

#ifndef CORE_TTRAITDATA_H_
#define CORE_TTRAITDATA_H_

#include <vector>
#include <string.h>
#include "stdint.h"
#include "newmat.h"
#include "newmatap.h"
#include "coretools/Files/TFile.h"
#include "coretools/Main/TLog.h"
#include "TTree.h"

//--------------------------------------------------------------------------
class TTraitDataOneDimension{
private:
	ColumnVector x_orig;
	ColumnVector x;
	uint32_t _matrixSize_s;
	Matrix x_transposed;
	double _s_0_squared, _log_s_0_squared;
	Matrix _ones;
	Matrix _ones_transposed;

	double _MulSym_xtMx(double* M) const;
	double SparseMulSim_xtMv(double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx);

public:
	TTraitDataOneDimension(const uint16_t & NumLeaves);
	~TTraitDataOneDimension();

	void setTrait(const uint16_t & LeaveIndex, const double & Trait);
	void setBrownianParameters(double RootState, double s_0_squared);
	ColumnVector getX(){ return x; };
	double getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log) const;
	void updateParametersEM(double & RootState, double & s_0_squared, const SymmetricMatrix & S) const;
	double hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx);
	double estimateNullModelMLE(double & RootState, double & s_0_squared, const SymmetricMatrix & Tau_inv, const double & Tau_det_log);
	void performFTest(const int & K, const Matrix & X, const SymmetricMatrix & Tau, const SymmetricMatrix & Tau_inv, const double & Tau_det_log);
};

class TTraitData{
private:
	std::vector<TTraitDataOneDimension> data;

public:
	TTraitData() = default;

	void readTraitFile(const std::string & Filename, const TTree & tree);
	TTraitDataOneDimension& operator[](uint16_t index){ return data[index]; };

	size_t dimensionality() const { return data.size(); };
	void setBrownianParameters(const std::vector<double> & RootStates, const std::vector<double> & s_0_squared);
	double getLogConditionalDensity(double* AlphaInvMatrixData, const double & TNAlpha_det_log);
	void updateParametersEM(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & S) const;
	double hastingsTerm(const double & alpha_times_delta_n_k, const double & r_k, double* AlphaInvMatrixData, int* PVectorsRel, const int & Pop, const int & LeafIdx, const int & StartIdx);
	double estimateNullModelMLE(std::vector<double> & RootStates, std::vector<double> & s_0_squared, const SymmetricMatrix & Tau_inv, const double & Tau_det_log);
	void performFTest(const int & K, const Matrix & X, const SymmetricMatrix & Tau, const SymmetricMatrix & Tau_inv, const double & Tau_det_log);
};



#endif /* CORE_TTRAITDATA_H_ */
