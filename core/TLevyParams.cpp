/*
 * TLevyParams.cpp
 *
 *  Created on: Jan 24, 2022
 *      Author: phaentu
 */


#include "TLevyParams.h"
#include "coretools/Files/TOutputFile.h"

//-------------------------------------
// TModelParamsBase
//-------------------------------------
double TModelParamsBase::logLikelihood() const {
	if(!_modelFitted){
		throw std::runtime_error("TNullModelParams::likelihood(): null model not yet fitted!");
	}
	return _logLikelihhood;
};

std::vector<double>& TModelParamsBase::rootStates(){
	return _rootStates;
};

std::vector<double>& TModelParamsBase::brownianVariances(){
	return _brownianVariances;
};

const std::vector<double>& TModelParamsBase::rootStates() const {
	return _rootStates;
};

const std::vector<double>& TModelParamsBase::brownianVariances() const {
	return _brownianVariances;
};

void TModelParamsBase::setRootStates(std::vector<double> & Rootstates){
	_rootStates = Rootstates;
	_modelFitted = false;
};

void TModelParamsBase::setBrownianVariance(std::vector<double> & BrownianVariances){
	_brownianVariances = BrownianVariances;
	_modelFitted = false;
};


void TModelParamsBase::_setFromParameters(const std::string & ParamName, const std::string & ParamNameFull, std::vector<double> & Param, size_t Dimension){
	coretools::instances::parameters().fillParameterIntoContainer(ParamName, Param, ',');
	if(Param.size() == 1 && Dimension > 1){
		Param.insert(Param.end(), Dimension, Param[0]);
	}
	coretools::instances::logfile().list("Setting " + ParamNameFull + " to ", coretools::str::concatenateString(Param, ", "), ". ('" + ParamName + ")");

	if(Param.size() != Dimension){
		throw "Number of " + ParamNameFull + " provided does not match dimensionality of the trait!";
	}
};

void TModelParamsBase::_setFromParameters(const std::string & ParamName, const std::string & ParamNameFull, std::vector<double> & Param, const std::vector<double> & NullModelMLE){
	if(coretools::instances::parameters().parameterExists(ParamName)){
		_setFromParameters(ParamName, ParamNameFull, Param, NullModelMLE.size());
	} else {
		Param = NullModelMLE;
		coretools::instances::logfile().list("Setting " + ParamNameFull + " to null model MLE. (use '" + ParamName + "' to provide)");
		coretools::instances::logfile().conclude(ParamNameFull + " = ", coretools::str::concatenateString(Param, ", "), ".");
	}
};

void TModelParamsBase::setRootStatesBrownianVariancesFromParameters(size_t Dimension){
	_setFromParameters("rootState", "root state(s)", _rootStates, Dimension);
	_setFromParameters("brownVar", "Brownian variance(s)", _brownianVariances, Dimension);
};

void TModelParamsBase::setRootStatesFromParameters(const std::vector<double> & NullModelMLE){
	_setFromParameters("rootState", "root state(s)", _rootStates, NullModelMLE);
};

void TModelParamsBase::setBrownianVariancesFromParameters(const std::vector<double> & NullModelMLE){
	_setFromParameters("brownVar", "Brownian variance(s)", _brownianVariances, NullModelMLE);
};

void TModelParamsBase::addParameterNames(std::vector<std::string> & Names, const std::string & Prefix) const {
	if(_rootStates.size() > 1){
		coretools::str::addExpandedIndex(Names, Prefix + "rootState_", _rootStates.size());
		coretools::str::addExpandedIndex(Names, Prefix + "brownianVariance_", _brownianVariances.size());
	} else {
		Names.push_back(Prefix + "rootState");
		Names.push_back(Prefix + "brownianVariance");
	}
};

void TModelParamsBase::write(coretools::TOutputFile & Out) const{
	Out << _rootStates << _brownianVariances;
};

void TModelParamsBase::report() const {
	coretools::instances::logfile().list("Root state = ", coretools::str::concatenateString(_rootStates, ", "));
	coretools::instances::logfile().list("Brownian variance = ", coretools::str::concatenateString(_brownianVariances, ", "));
	coretools::instances::logfile().list("log-likelihood = ", _logLikelihhood);
};

//-------------------------------------
// TNullModelParams
//-------------------------------------
void TNullModelParams::fit(TVariables* Vars){
	_logLikelihhood = Vars->computeNullModelMLE(_rootStates, _brownianVariances);
	_modelFitted = true;
};

//------------------------------------------------
// TlevyParams
//------------------------------------------------
TlevyParams::TlevyParams(){
	_modelFitted = false;
	_logLikelihhood = 0.0;
	_FisherInfo = 0.0;
	_alpha = 0.0;
	_lambda = 0.0;
};

TlevyParams::TlevyParams(double Alpha){
	_modelFitted = false;
	_logLikelihhood = 0.0;
	_FisherInfo = 0.0;
	_alpha = Alpha;
	_lambda = 0.0;
};

TlevyParams::TlevyParams(size_t Dimension){
	setFromParameters(Dimension);
};

double TlevyParams::FisherInfo() const {
	if(!_modelFitted){
		throw std::runtime_error("TlevyParams::FisherInfo(): null model not yet fitted!");
	}
	return _FisherInfo;
};

double TlevyParams::alpha() const {
	return _alpha;
};

double TlevyParams::lambda() const {
	return _lambda;
};

void TlevyParams::setAlpha(double Alpha){
	_alpha = Alpha;
	_modelFitted = false;
};

void TlevyParams::setLambda(double Lambda){
	_lambda = Lambda;
	_modelFitted = false;
};

void TlevyParams::setModelfit(double LogLikelihood, double FisherInfo){
	_logLikelihhood = LogLikelihood;
	_FisherInfo = FisherInfo;
	_modelFitted = true;
};

void TlevyParams::setFromParameters(size_t Dimension){
	TModelParamsBase::setRootStatesBrownianVariancesFromParameters(Dimension);
	_lambda = coretools::instances::parameters().getParameter<double>("lambda");
	coretools::instances::logfile().list("Setting jump rate lambda to ", _lambda, ". ('lambda')");
	_alpha = coretools::instances::parameters().getParameter<double>("alpha");
	coretools::instances::logfile().list("Setting alpha to ", _alpha, ". ('alpha')");
};

void TlevyParams::setLambdaFromParameters(const TTree & Tree){
	if(coretools::instances::parameters().parameterExists("lambda")){
		_lambda = coretools::instances::parameters().getParameter<double>("lambda");
		coretools::instances::logfile().list("Setting jump rate lambda to ", _lambda, ". ('lambda')");
	} else {
		_lambda = 10.0 / Tree.getTotalTreeLength();
		coretools::instances::logfile().list("Setting jump rate lambda such that 10 events are expected on the tree.");
		coretools::instances::logfile().conclude("lambda = 10 / ", Tree.getTotalTreeLength(), " = ", _lambda);
	}
};

void TlevyParams::addParameterNames(std::vector<std::string> & Names, const std::string & Prefix, bool includeAlpha) const {
	if(includeAlpha){
		Names.push_back(Prefix + "alpha");
	}
	Names.push_back(Prefix + "lambda");
	TModelParamsBase::addParameterNames(Names, Prefix);
};

void TlevyParams::write(coretools::TOutputFile & Out) const {
	Out << _alpha << _lambda;
	TModelParamsBase::write(Out);
};

void TlevyParams::report() const {
	coretools::instances::logfile().list("Alpha = ", _alpha);
	coretools::instances::logfile().list("Lambda = ", _lambda);
	TModelParamsBase::report();
};


void TlevyParams::writeHeaderToLogTable() const {
	coretools::instances::logfile().flushFixedWidth("lambda", 12);
	for(size_t i = 0; i < _rootStates.size(); ++i){
		coretools::instances::logfile().flushFixedWidth("rootState_" + coretools::str::toString(i+1), 14);
	}
	for(size_t i = 0; i < _brownianVariances.size(); ++i){
		coretools::instances::logfile().flushFixedWidth("brownVar_" + coretools::str::toString(i+1), 14);
	}
};

void TlevyParams::writeToLogTable() const {
	coretools::instances::logfile().flushScientific(_lambda, 3, 12);
	for(auto& it : _rootStates){
		coretools::instances::logfile().flushScientific(it, 3, 14);
	}
	for(auto& it : _brownianVariances){
		coretools::instances::logfile().flushScientific(it, 3, 14);
	}
};


//------------------------------------------------
// TModelChoice
//------------------------------------------------
TModelChoice::TModelChoice(const TNullModelParams & NullModel){
	_nullModelLikelihood = NullModel.logLikelihood();
};

void TModelChoice::performModelchoice(const TlevyParams & LevyParams){
	//Likelihood ratio test
	_levyLikelihood = LevyParams.logLikelihood();
	_LRT_statistics = -2.0*(_nullModelLikelihood - _levyLikelihood);
	//compare to chi-sq table (too lazy to implement chi-sq, as it is anyway just and approximation)
	//interpolate between entries
	if(_LRT_statistics<=0.0) _LRT_pValue=1.0;
	else {
		if(_LRT_statistics > 73.4736010){
			_LRT_pValue = 1e-16;
		} else {
			int x=0;
			while(chisq[x]<_LRT_statistics){
				++x;
			}
			_LRT_pValue=pvalue[x] + (chisq[x]-_LRT_statistics)/(chisq[x]-chisq[x-1])*(pvalue[x-1]-pvalue[x]);
		}
	}

	//AIC
	_AIC_difference=(4.0 - 2.0 * _nullModelLikelihood) - (8.0 - 2.0 * _levyLikelihood);
};

void TModelChoice::performModelchoiceAndReport(const TlevyParams & LevyParams){
	performModelchoice(LevyParams);
	report();
};

void TModelChoice::convergedAtNullModel(){
	_levyLikelihood = _nullModelLikelihood;
	_LRT_statistics = 0.0;
	_LRT_pValue = 1.0;
	_AIC_difference = 0.0;
};

void TModelChoice::addParameterNames(std::vector<std::string> & Names) const {
	Names.insert(Names.end(), {"LL", "null_LL", "LRT_statistics", "LRT_pValue", "preferredModelLRT", "AIC_difference", "preferredModelAIC"});
};

std::string TModelChoice::_choice(bool IsLevy) const {
	if(IsLevy){
		return "Lévy";
	} else {
		return "Null";
	}
};

void TModelChoice::write(coretools::TOutputFile & Out) const {
	Out.write(_levyLikelihood, _nullModelLikelihood, _LRT_statistics, _LRT_pValue, _choice(_LRT_pValue < 0.05), _AIC_difference, _choice(_AIC_difference > 0));
};

void TModelChoice::_reportChoice(bool IsLevy) const {
	if(IsLevy){
		coretools::instances::logfile().conclude("Lévy model is preferred");
	} else {
		coretools::instances::logfile().conclude("Null model is preferred");
	}
};

void TModelChoice::report() const {
	coretools::instances::logfile().startIndent("Model choice statistics:");

	coretools::instances::logfile().list("Likelihood of null model = ", _nullModelLikelihood);
	coretools::instances::logfile().list("Likelihood of Lévy model = ", _levyLikelihood);

	//Likelihood ratio test
	coretools::instances::logfile().list("Likelihood-Ratio-Test D = ", _LRT_statistics, " (chi-square approx. p.value = ", _LRT_pValue, " )");
	_reportChoice(_LRT_pValue <= 0.05);

	//AIC
	coretools::instances::logfile().list("Difference in Akaike information criterion = ", _AIC_difference, " (null model - Lévy model)");
	_reportChoice(_AIC_difference > 0);

	coretools::instances::logfile().endIndent();
};


