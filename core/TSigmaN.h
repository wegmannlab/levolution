/*
 * TSigmaN.h
 *
 *  Created on: Jul 2, 2011
 *      Author: wegmannd
 */

#ifndef TSIGMAN_H_
#define TSIGMAN_H_

#include "TTraitData.h"
#include "TTree.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Main/TParameters.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "coretools/Files/TFile.h"
#include "TVariables.h"
#include "TLevyParams.h"

//--------------------------------------------------------------------------
//A DB to record the position of jumps during an MCMC
class TJumpDB{
public:
	TTree* tree;
	bool DBInitialized;
	long numRecords;

	//DB for branch specific posteriors
	int numBranches;
	int* jumpDBBranches_hasJump; //used to store how often each branch had at least one jump
	int* jumpDBBranches_numJumps; //used to store posterior mean on the number if jumps per branch

	//DB for leave specific posterior
	int numLeaves;
	std::vector<int>* branchToLeaveMap; //mapping branch indexes to the leaves for which this branch is parent.
	int* jumpDBLeaves_tmp;
	int* jumpDBLeaves_hasJump; //used to store how often the branches from the root to the tips have at least one jump
	int* jumpDBLeaves_numJumps; //used to store posterior mean on the number from the root to the tips


	TJumpDB();
	~TJumpDB(){
		if(DBInitialized){
			delete[] jumpDBBranches_hasJump;
			delete[] jumpDBBranches_numJumps;
			delete[] branchToLeaveMap;
			delete[] jumpDBLeaves_tmp;
			delete[] jumpDBLeaves_hasJump;
			delete[] jumpDBLeaves_numJumps;
		}
	};
	void emptyDB(TTree* Tree);
	void updateJumpDB(std::vector<int> & branchesWithJumps, int* jumpsPerBranch);
	std::string getPosteriorTree_hasJumps();
	std::string getPosteriorTree_numJumps();
	std::string getPosteriorTree_lengthWithJumps(const double alpha);
	std::string getPosteriorLeaves_hasJumps();
	std::string getPosteriorLeaves_numJumps();
};

//--------------------------------------------------------------------------
class TSigmaN{
public:
	TTree* tree;
	int numBranches;
	int numLeaves;
	TVariables* vars;
	int* jumpsPerBranch;
	std::vector<int> branchesWithJumps;
	int numJumpsOnTree;
	//double* traits_x;

	//alpha stuff
	double alpha, lambda;
	SymmetricMatrix TNAlpha_inv;
	double* AlphaInvMatrixData;
	double* PVectorAlphaTemp;
	double TNAlpha_det_log;

	//variables for MCMC step (always overwritten during each step)
	//int chosenBranch_k, delta_n_k, n_k_plus;
	int Pop, LeafIdx, StartIdx;
	bool* delta_n_kBool;
	double* randomForHastings;
	int randomIndex;
	int numAccepted;

	TSigmaN(TTree* Tree, TVariables* Vars);
	TSigmaN(TSigmaN* other);
	virtual ~TSigmaN(){
		delete[] jumpsPerBranch;
		delete[] PVectorAlphaTemp;
		delete[] randomForHastings;
		delete[] delta_n_kBool;
	};

	virtual void reset(const TlevyParams & LevyParams);
	bool updateJumpsOnBranchDB(const int & branch, const int & jumps);
	void addJumpsToBranch(const int & branch, const int & jumps);
	void addJumpsToRandomBranchesGeometric(double & probGeometric);
	void setJumpsFrom(TSigmaN* other);
	bool hasSameJumps(TSigmaN* other);
	void writePoissonTree(std::string filename);
	int getTotalNumberOfJumps();
	void addJumpsToDB(TJumpDB & DB);
	std::string getJumpConfigForPrinting();
	void computeTNAlpha();
	void updateTNAlphaIteratively(const int & branch, const double delta_n_k);
	void fillPVectorAlphaTemp();
	double SparseMulSim_vtMv();
	double SparseMulSim_xtMv();
	virtual void SparseMulSimAdd_MvMvt(double Multiplier);
	virtual void fastMultipleAddMatricesSim(){throw "Function fastMultipleAddMatricesSim not defined in TSigmaN!";};

	double getLogConditionalDensity();
	double getLogPriorDensityOfJumps();
	double getLogLikelihood();
	double calculateHastingForGeometricTransition(TSigmaN* other, double & probGeometric);
	double calculatePriorRatioOneBranch(int & branch, TSigmaN* other);
	void prepareMCMCStep();
	int performMCMCStepForEM(const double heat = 1.0, const bool verbose = false);
	int performMCMCStepForLL(const double heat = 1.0, const bool verbose = false);
};

class TSigmaN_EM:public TSigmaN{
public:
	SymmetricMatrix S;
	double* PMatrixS;
	double SMultiplyCoef;

	TSigmaN_EM(TTree* Tree, TVariables* Vars);
	TSigmaN_EM(TSigmaN_EM* other);
	~TSigmaN_EM(){};
	void reset(const TlevyParams & LevyParams);
	void SparseMulSimAdd_MvMvt(double Multiplier);
	void fastMultipleAddMatricesSim();
};


#endif /* TSIGMAN_H_ */
