/*
 * TMCMC.h
 *
 *  Created on: Sep 16, 2014
 *      Author: wegmannd
 */

#ifndef TMCMC_H_
#define TMCMC_H_

#include "TTree.h"
#include "coretools/Main/TRandomGenerator.h"
#include "TSigmaN.h"
#include "coretools/Main/TLog.h"
#include "TLevyParams.h"

//-------------------------------------
// TMCMCSettings
//-------------------------------------
class TMCMCSettings{
private:
	uint32_t _numNVectors {0};
	uint32_t _thinning {0};
	uint32_t _burninLength {0};
	uint32_t _totalLength {0};
	bool _initialized {false};

	void _setTotalLength();

public:
	TMCMCSettings() = default;
	~TMCMCSettings() = default;

	uint32_t numNVectors() const { return _numNVectors; };
	uint32_t thinning() const { return _thinning; };
	uint32_t burninLength() const { return _burninLength; };
	uint32_t totalLength() const { return _totalLength; };
	bool initialized() const { return _initialized; };

	void initialize(const std::string & Postfix = "", uint32_t DefaultNumNVectors = 5000, uint32_t DefaultThinning = 10, uint32_t DefaultBurninLength = 1000);
};

//------------------------------------------------
// TMCMCBase
//------------------------------------------------
class TMCMCBase{

protected:
	int prog, oldProg;
	coretools::TTimer timer;

	TlevyParams* levyParams;
	TTree* tree;
	TVariables* vars;
	bool SigmaVectorInitialized;
	double heat;
	double acceptanceRate;
	long numAccepted;
	bool verboseProgressConclusion;

	void startProgressReport();
	void reportProgress(uint32_t iteration, uint32_t length);
	void concludeProgressReport(uint32_t length);
	virtual void concludeAcceptance(uint32_t length);

public:
	TSigmaN* curSigmaNvector;

	TMCMCBase(TTree* Tree, TVariables* Vars);
	virtual ~TMCMCBase(){
		if(SigmaVectorInitialized) delete curSigmaNvector;
	};
	virtual void reset(TlevyParams & LevyParams);
	virtual void initCurSigmaNvector();
	void setHeat(double Heat){heat = Heat;};
	double getHeat() const { return heat; };
	double getCurrentLogLikelihood(){return curSigmaNvector->getLogLikelihood(); };

	virtual void prepareMCMCStep();
	virtual void runMCMCStepForEM(const bool verbose = false);
	virtual void runMCMCStepForLL(const bool verbose = false);

	void setProgressConclusionVerbosity(const bool & Verbosity){
		verboseProgressConclusion = Verbosity;
	};
};

//-------------------------------------------------------------------
class TMCMC:public TMCMCBase{
//-------------------------------------------------------------------
public:
	TSigmaN_EM* curSigmaNvectorEM;

	TMCMC(TTree* Tree, TVariables* Vars);
	void initCurSigmaNvector();
	void runMCMC(const TMCMCSettings & settings);
	void runMCMCForConvergenceTesting(const TMCMCSettings & settings, std::ofstream & outputFile);
};

//-------------------------------------------------------------------
class TMCMCLL:public TMCMCBase{
//-------------------------------------------------------------------
private:
	double _normalizer;
public:
	TMCMCLL(TTree* Tree, TVariables* Vars, double normalizer);
	void runMCMC(const TMCMCSettings & settings);
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayes:public TMCMCBase{
//-------------------------------------------------------------------
protected:
	void writeIntermediatePosteriorTrees(std::string & outname, uint32_t iteration, uint32_t length);

public:
	TJumpDB jumpDB;

	TMCMCEmpiricalBayes(TTree* Tree, TVariables* Vars);
	virtual void runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname);
	void writeJumpPosteriors(std::string basename, bool verbose);
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayesLongJumps:public TMCMCEmpiricalBayes{
//-------------------------------------------------------------------
protected:
	void concludeAcceptance(uint32_t length);

public:
	double probLongJump;
	double probGeometric;
	double acceptanceRateLongJumps;

	TMCMCEmpiricalBayesLongJumps(TTree* Tree, TVariables* Vars, double ProbLongJump, double ProbGeometric);
	void runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname);
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayesHeated:public TMCMCEmpiricalBayes{
//A heated version that does not recorded anything beyond its state
//-------------------------------------------------------------------
private:
	void concludeAcceptance(uint32_t length);

public:
	int numChains;
	TMCMCBase** heatedChains;
	double acceptanceRateSwaps;
	int numSwaps;

	TMCMCEmpiricalBayesHeated(TTree* Tree, TVariables* Vars, int NumChains, double DeltaT);
	~TMCMCEmpiricalBayesHeated(){
		for(int i=1; i<numChains; ++i) delete heatedChains[i];
		delete[] heatedChains;
	};
	void reset(TlevyParams & LevyParams);
	void initCurSigmaNvector();
	bool swapChains(TMCMCBase* first, TMCMCBase* second);
	void runMCMC(const TMCMCSettings & settings, int & intermediatePosteriorThinning, std::string & outname);
};


#endif /* TMCMC_H_ */
