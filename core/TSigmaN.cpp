/*
 * TSigmaN.cpp
 *
 *  Created on: Jul 2, 2011
 *      Author: wegmannd
 */

#include "TSigmaN.h"


//-------------------------------------------------------------
//TJumpDB
//-------------------------------------------------------------
TJumpDB::TJumpDB(){
	tree = NULL;
	numRecords = 0;
	DBInitialized = false;

	numBranches = 0;
	jumpDBBranches_hasJump = NULL;
	jumpDBBranches_numJumps = NULL;

	numLeaves = 0;
	branchToLeaveMap = NULL;
	jumpDBLeaves_tmp = NULL;
	jumpDBLeaves_hasJump = NULL;
	jumpDBLeaves_numJumps = NULL;
};

void TJumpDB::emptyDB(TTree* Tree){
	if(!DBInitialized){
		tree = Tree;

		//banch specific DB
		numBranches = tree->getNumNodes();
		jumpDBBranches_hasJump = new int[numBranches];
		jumpDBBranches_numJumps = new int[numBranches];

		//fill branch to leave map
		numLeaves = tree->getNumLeaves();
		branchToLeaveMap = new std::vector<int>[numBranches];
		for(int b=0; b<numBranches; ++b){
			for(int l=0; l<numLeaves; ++l){
				if(tree->isSubordinate(b, l)){
					branchToLeaveMap[b].push_back(l);
				}
			}
		}

		//leave specific DB
		jumpDBLeaves_tmp = new int[numLeaves];
		jumpDBLeaves_hasJump = new int[numLeaves];
		jumpDBLeaves_numJumps = new int[numLeaves];
		DBInitialized = true;
	}
	for(int b=0; b<numBranches; ++b){
		jumpDBBranches_hasJump[b] = 0;
		jumpDBBranches_numJumps[b] = 0;
	}
	for(int l=0; l<numLeaves; ++l){
		jumpDBLeaves_hasJump[l] = 0;
		jumpDBLeaves_numJumps[l] = 0;
	}
	numRecords = 0;
};

void TJumpDB::updateJumpDB(std::vector<int> & branchesWithJumps, int* jumpsPerBranch){
	//prepare tmp
	for(int l=0; l<numLeaves; ++l){
		jumpDBLeaves_tmp[l] = 0;
	}

	//loop over branches with jumps
	for(auto& it : branchesWithJumps){
		//update branch specific DB
		++jumpDBBranches_hasJump[it];
		jumpDBBranches_numJumps[it] += jumpsPerBranch[it];

		//store leave specific jumps
		for(auto& l : branchToLeaveMap[it]){
			jumpDBLeaves_tmp[l] += jumpsPerBranch[it];
		}
	}

	//update leave specific DB
	for(int l=0; l<numLeaves; ++l){
		if(jumpDBLeaves_tmp[l] > 0){
			++jumpDBLeaves_hasJump [l];
			jumpDBLeaves_numJumps[l] += jumpDBLeaves_tmp[l];
		}
	}

	//increase records
	++numRecords;
};

std::string TJumpDB::getPosteriorTree_hasJumps(){
	TTree outTree(*tree);
	outTree.setValue(0.0);
	for(int i=0; i<numBranches; ++i){
		outTree.setValueOfBranch(i, (double) jumpDBBranches_hasJump[i] / (double) numRecords);
	}
	return outTree.getNewickValues();
};

std::string TJumpDB::getPosteriorTree_numJumps(){
	TTree outTree(*tree);
	outTree.setValue(0.0);
	for(int i=0; i<numBranches; ++i){
		outTree.setValueOfBranch(i, (double) jumpDBBranches_numJumps[i] / (double) numRecords);
	}
	return outTree.getNewickValues();
};

std::string TJumpDB::getPosteriorTree_lengthWithJumps(const double alpha){
	TTree outTree(*tree);
	for(int i=0; i<numBranches; ++i){
		outTree.addValueToBranch(i, alpha * (double) jumpDBBranches_numJumps[i] / (double) numRecords);
	}
	return outTree.getNewickValues();
};

std::string TJumpDB::getPosteriorLeaves_hasJumps(){
	std::string s;
	for(int l=0; l<numLeaves; ++l){
		s += tree->getLeaveName(l) + "\t" + coretools::str::toString((double) jumpDBLeaves_hasJump[l] / (double) numRecords) + "\n";
	}
	return s;
};

std::string TJumpDB::getPosteriorLeaves_numJumps(){
	std::string s;
	for(int l=0; l<numLeaves; ++l){
		s += tree->getLeaveName(l) + "\t" + coretools::str::toString((double) jumpDBLeaves_numJumps[l] / (double) numRecords) + "\n";
	}
	return s;
};

//-------------------------------------------------------------
//TSigmaN
//-------------------------------------------------------------
TSigmaN::TSigmaN(TTree* Tree, TVariables* Vars){
	tree = Tree;
	vars = Vars;
	numBranches = tree->getNumNodes();
	numLeaves = tree->getNumLeaves();
	jumpsPerBranch = new int[numBranches];

	//init alpha stuff
	alpha = 0;
	lambda = 0;
	PVectorAlphaTemp = new double [numLeaves];
	AlphaInvMatrixData = NULL;
	TNAlpha_det_log = 0;
	numJumpsOnTree = 0;

	//initialize variables for MCMC
	Pop = 0;
	LeafIdx = 0;
	StartIdx = 0;
	delta_n_kBool = new bool[numBranches];
	randomForHastings = new double[numBranches];
	randomIndex = numBranches; //initially, fill all random variables
	numAccepted = 0;
};

TSigmaN::TSigmaN(TSigmaN* other){
	tree = other->tree;
	vars = other->vars;
	numBranches = other->numBranches;
	numLeaves = other->numLeaves;

	//copy alpha stuff
	alpha = other->alpha;
	lambda = other->lambda;
	TNAlpha_inv = other->TNAlpha_inv;
	AlphaInvMatrixData = TNAlpha_inv.Store();
	TNAlpha_det_log = other->TNAlpha_det_log;
	numJumpsOnTree = other->numJumpsOnTree;

	//copy variables for MCMC
	Pop = other->Pop;
	LeafIdx = other->LeafIdx;
	StartIdx = other->StartIdx;
	randomIndex = numBranches; //initially, fill all random variables
	numAccepted = other->numAccepted;

	//copy arrays
	jumpsPerBranch = new int[numBranches];
	delta_n_kBool = new bool[numBranches];
	randomForHastings = new double[numBranches];
	for(int i=0; i<numBranches; ++i){
		jumpsPerBranch[i] = other->jumpsPerBranch[i];

		delta_n_kBool[i] = other->delta_n_kBool[i];
		randomForHastings[i] = other->randomForHastings[i];
	}
	PVectorAlphaTemp = new double [numLeaves];
	for(int i=0; i<numLeaves; ++i)
		PVectorAlphaTemp[i] = other->PVectorAlphaTemp[i];
};

void TSigmaN::updateTNAlphaIteratively(const int & branch, const double delta_n_k){
	Pop = vars->PVectorsPop[branch];
	StartIdx = vars->PVectorsIdx[branch];
	LeafIdx = tree->nodes[branch]->LeafIdx;

	double r_k = 1.0 + alpha * delta_n_k * SparseMulSim_vtMv();
	double Multiplier = -alpha * delta_n_k / r_k;
	SparseMulSimAdd_MvMvt(Multiplier);
	TNAlpha_det_log += log(r_k);
};

void TSigmaN::computeTNAlpha(){
	//compute the inverse and determinant of TNAlpha(n)
	//we use an iterative scheme to do so (see notes)
	TNAlpha_inv = vars->Tau_inv;
	TNAlpha_det_log = vars->Tau_det_log;
	AlphaInvMatrixData = TNAlpha_inv.Store();

	for(std::vector<int>::iterator i=branchesWithJumps.begin(); i!=branchesWithJumps.end(); ++i){
		for(int j = 0; j<jumpsPerBranch[*i]; ++j)
			updateTNAlphaIteratively(*i, 1.0);
	}
};

void TSigmaN::fillPVectorAlphaTemp(){
	double ResSum;
	int k,l;
	for(int i=0;i<numLeaves;i++){
		ResSum = 0;
		l = (i*(i+1)) >> 1;
		for(int j=0;j<Pop;j++){
			k = vars->PVectorsRel[StartIdx+j];
			if (k > i) ResSum += AlphaInvMatrixData[((k*(k+1)) >> 1)+i];
			else ResSum += AlphaInvMatrixData[l+k];
		}
		PVectorAlphaTemp[i] = ResSum;
	}
};

void TSigmaN::reset(const TlevyParams & LevyParams){
	numJumpsOnTree = 0;
	branchesWithJumps.clear();
	//save the number of poisson jumps per branch, which is used to compute the poisson probability later
	for(int i=0; i<numBranches; ++i){
		jumpsPerBranch[i]=0;
	}
	//set levy parameters and initialize TNAlpha
	alpha = LevyParams.alpha();
	lambda = LevyParams.lambda();

	//set parameters in vars
	vars->setBrownianParameters(LevyParams.rootStates(), LevyParams.brownianVariances());

	TNAlpha_inv = vars->Tau_inv;
	TNAlpha_det_log = vars->Tau_det_log;
	AlphaInvMatrixData = TNAlpha_inv.Store();

	randomIndex = numBranches; //initially, fill all random variables
};

bool TSigmaN::updateJumpsOnBranchDB(const int & branch, const int & jumps){
	//update entries
	if(jumpsPerBranch[branch] + jumps < 0) return false;
	numJumpsOnTree += jumps;
	if(jumpsPerBranch[branch] == 0){
		jumpsPerBranch[branch] = jumps;
		branchesWithJumps.push_back(branch);
	} else {
		jumpsPerBranch[branch] += jumps;
		if(jumpsPerBranch[branch] == 0){
			for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it){
				if(*it == branch){
					branchesWithJumps.erase(it);
					break;
				}
			}
		}
	}
	return true;
};

void TSigmaN::addJumpsToBranch(const int & branch, const int & jumps){
	//This function adds jumps to branches and updates TNAlpha using the iterative fashion
	if(updateJumpsOnBranchDB(branch, jumps)){
		double delta_n_k = 1.0;
		if(jumps < 0) delta_n_k = -1.0;

		//if multiple jumps, do one at a time
		for(int j=1; j<=abs(jumps); ++j){
			updateTNAlphaIteratively(branch, delta_n_k);
		}
	}
};

void TSigmaN::addJumpsToRandomBranchesGeometric(double & probGeometric){
	int numJumps = coretools::instances::randomGenerator().getGeometricRandom(probGeometric);
	numJumps = 1;

	int branch;
	for(int i=0; i<numJumps; ++i){
		branch = tree->getRandomNode();
		updateJumpsOnBranchDB(branch, 1.0);
		updateTNAlphaIteratively(branch, 1.0);
	}
};

void TSigmaN::setJumpsFrom(TSigmaN* other){
	TNAlpha_inv = vars->Tau_inv;
	TNAlpha_det_log = vars->Tau_det_log;
	AlphaInvMatrixData = TNAlpha_inv.Store();

	for(std::vector<int>::iterator i=other->branchesWithJumps.begin(); i!=other->branchesWithJumps.end(); ++i){
		if(updateJumpsOnBranchDB(*i, other->jumpsPerBranch[*i])){
			for(int j = 0; j<other->jumpsPerBranch[*i]; ++j){
				updateTNAlphaIteratively(*i, 1.0);
			}
		}
	}
};

bool TSigmaN::hasSameJumps(TSigmaN* other){
	for(std::vector<int>::iterator i=branchesWithJumps.begin(); i!=branchesWithJumps.end(); ++i){
		if(jumpsPerBranch[*i] != other->jumpsPerBranch[*i]) return false;
	}
	for(std::vector<int>::iterator i=other->branchesWithJumps.begin(); i!=other->branchesWithJumps.end(); ++i){
		if(jumpsPerBranch[*i] != other->jumpsPerBranch[*i]) return false;
	}
	return true;
};

int TSigmaN::getTotalNumberOfJumps(){
	return numJumpsOnTree;
};

void TSigmaN::addJumpsToDB(TJumpDB & DB){
	DB.updateJumpDB(branchesWithJumps, jumpsPerBranch);
};

void TSigmaN::writePoissonTree(std::string filename){
	tree->setJumps(0);
	for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it)
		tree->setNumJumpsOnBranch(*it, jumpsPerBranch[*it]);

	//write to file
	std::ofstream out(filename.c_str());
	out << tree->getNewickJumps() << std::endl;
	out.close();
};

std::string TSigmaN::getJumpConfigForPrinting(){
	return "(" + coretools::str::concatenateString(branchesWithJumps, ",") + ")";
};

double TSigmaN::SparseMulSim_vtMv(){
	if(LeafIdx > -1)
		return AlphaInvMatrixData[(LeafIdx*(LeafIdx+3))/2];

	int j,k,l,m;
	double ResSum = 0.0;

	if (Pop < 9){
		int ad[8];
		j =  StartIdx;
		for(int i=0; i<Pop; i++, ++j){
			ad[i] = vars->PVectorsRel[j];
			ad[i] = (ad[i]*ad[i]+ad[i]) >> 1;
		}

		for(int i=0;i<Pop;i++){
			m = ad[i];
			for(j=0;j<=i;j++){
				ResSum += AlphaInvMatrixData[m+vars->PVectorsRel[StartIdx+j]];
			}
			for(j=i+1;j<Pop;j++){
				m = ad[j];
				ResSum += AlphaInvMatrixData[m+vars->PVectorsRel[StartIdx+i]];
			}
		}
		return ResSum;
	}

	for(int i=0;i<Pop;i++){
		k = vars->PVectorsRel[StartIdx+i];
		m = (k*k+k)/2;
		for(j=0;j<Pop;j++){
			l = vars->PVectorsRel[StartIdx+j];
			if (k>l) ResSum += AlphaInvMatrixData[m+l];
			else ResSum += AlphaInvMatrixData[(l*l+l)/2+k];
		}
	}
	return ResSum;
};

void TSigmaN::SparseMulSimAdd_MvMvt(double Multiplier){
	//version that does not update PMatrixS
	double ResSum;
	int Idx;

	fillPVectorAlphaTemp();

	for(int i=0;i<numLeaves;i++){
		if (PVectorAlphaTemp[i] != 0){
			ResSum = Multiplier*PVectorAlphaTemp[i];
			Idx = (i*(i+1))/2;
			for(int j=0;j<=i;j++){
				AlphaInvMatrixData[Idx+j] += ResSum*PVectorAlphaTemp[j];
			}
		}
	}
};

double TSigmaN::getLogConditionalDensity(){
	return vars->getLogConditionalDensity(AlphaInvMatrixData, TNAlpha_det_log);
};

double TSigmaN::getLogPriorDensityOfJumps(){
	//Is product of poisson probabilities for each branch
	double priorDens = -lambda*tree->getTotalTreeLength() + log(lambda)*numJumpsOnTree;
	for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it){
		if(jumpsPerBranch[*it] == 1)
			priorDens += log(tree->getBranchLength(*it));
		else {
			priorDens += jumpsPerBranch[*it] * log(tree->getBranchLength(*it));
			int tmp = 2;
			for(int i=3; i<jumpsPerBranch[*it]; ++i) tmp *= i;
			priorDens -= log(tmp);
		}
	}
	return priorDens;
};

double TSigmaN::getLogLikelihood(){
	return getLogConditionalDensity() + getLogPriorDensityOfJumps();
};

double TSigmaN::calculateHastingForGeometricTransition(TSigmaN* other, double & probGeometric){
	//ratio of likelihood
	double h = getLogConditionalDensity() / other->getLogConditionalDensity();

	//ratio of prior on lambda -> only calculate for branches with different number of jumps!
	double prior = 1.0;
	if(numJumpsOnTree + numJumpsOnTree*numJumpsOnTree < 2*numBranches){
		//assemble list of branches with unequal number of jumps
		std::vector<int> branchList;
		for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it){
			branchList.push_back(*it);
		}
		//add those of other tree that are missing
		bool found;
		for(std::vector<int>::iterator itOther=other->branchesWithJumps.begin(); itOther!=other->branchesWithJumps.end(); ++itOther){
			//check if they are missing
			found = false;
			for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it){
				if(*it == *itOther){
					found = true;
					break;
				}
			}
			if(!found) branchList.push_back(*itOther);
		}

		//Now go through these branches to calculate ratio of prior
		for(std::vector<int>::iterator it=branchList.begin(); it!=branchList.end(); ++it){
			if(jumpsPerBranch[*it] != other->jumpsPerBranch[*it]){
				prior *= calculatePriorRatioOneBranch(*it, other);
			}
		}
	} else {
		for(int b=0; b<numBranches; ++b){
			if(jumpsPerBranch[b] != other->jumpsPerBranch[b]){
				prior *= calculatePriorRatioOneBranch(b, other);
			}
		}
	}
	h *= prior;

	//add transition prob: P(|n| | geometric) * P(n | |n| )
	int nPrime = getTotalNumberOfJumps();
	int n = other->getTotalNumberOfJumps();
	int B = other->numBranches;
	if(n != nPrime){
		h *= pow(1.0 - probGeometric,  n - nPrime) * pow(1.0/(double) B, n - nPrime);
		//add difference in factorial
		if(nPrime < n){
			for(int i = n; i > nPrime; --i) h *= (double) i;
		} else {
			for(int i = nPrime; i > n; --i) h /= (double) i;
		}
	}
	//add difference in distribution: old
	for(std::vector<int>::iterator itOther=other->branchesWithJumps.begin(); itOther!=other->branchesWithJumps.end(); ++itOther){
		if(other->jumpsPerBranch[*itOther] > 1){
			for(int i=other->jumpsPerBranch[*itOther]; i > 1; --i) h /= (double) i;
		}
	}

	//add difference in distribution: new
	for(std::vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it){
		if(jumpsPerBranch[*it] > 1){
			for(int i=jumpsPerBranch[*it]; i > 1; --i) h *= (double) i;
		}
	}
	return h;
};

double TSigmaN::calculatePriorRatioOneBranch(int & branch, TSigmaN* other){
	double res = pow(lambda*tree->getBranchLength(branch), jumpsPerBranch[branch] - other->jumpsPerBranch[branch]);
	int fac = 1;
	if(jumpsPerBranch[branch] < other->jumpsPerBranch[branch]){
		for(int i=other->jumpsPerBranch[branch]; i > jumpsPerBranch[branch]; --i)
			fac *= i;
		res *= (double) fac;
	} else {
		for(int i=jumpsPerBranch[branch]; i > other->jumpsPerBranch[branch]; --i)
			fac *= i;
		res /= (double) fac;
	}
	return res;
};

void TSigmaN::prepareMCMCStep(){
	//need to do this before in order to parallelize
	//1) Store a single random number to be used in a binary fashion to get
	//delta_n_k for each branch;
	int rand = coretools::instances::randomGenerator().getRand() * 32768;
	int index = 0;

	while(index < numBranches){
		rand = coretools::instances::randomGenerator().getRand() * 32768;
		for (int i = 0; i < 15; i++){
			delta_n_kBool[index] = (rand >> i) & 1;
			++index;
			if(index == numBranches) break;
		}
	}

	//2) store random number for hastings
	//But: only refill those random numbers used!
	for(int i=0; i<randomIndex; ++i)
		randomForHastings[i] = coretools::instances::randomGenerator().getRand();
};

int TSigmaN::performMCMCStepForEM(const double heat, bool verbose){
	//This function performs the simple MCMC step outlined in the notes
	//The basic idea is to always only add or remove one jump on a random branch,
	//which allows us to speed up the calculation of TNAlpha
	//The MCMC for EM samples from P(n|x,s_0^2, alpha, lambda)

	//per iteration, each branch will be updated once
	numAccepted = 0;
	randomIndex = 0;
	double delta_n_k;

	if(verbose) { std::cout << "HASTINGS [alpha = " << alpha << "]: " << std::flush; }

	for(int chosenBranch_k = 0; chosenBranch_k < numBranches; ++chosenBranch_k){
		//ignore branches of length 0
		if(tree->getBranchLength(chosenBranch_k) > 0.0){

			//delta_n_k is either -1.0 or 1.0 with prob 1/2
			delta_n_k = -1.0 + 2.0 * delta_n_kBool[chosenBranch_k];

			//Calculate hastings ratio (only if move results in non-negative number jumps!)
			if(jumpsPerBranch[chosenBranch_k] + delta_n_k >= 0.0){

				//prepare iterators
				Pop = vars->PVectorsPop[chosenBranch_k];
				StartIdx = vars->PVectorsIdx[chosenBranch_k];
				LeafIdx = tree->nodes[chosenBranch_k]->LeafIdx;

				//calculate hastings
				double r_k = 1.0 + alpha * delta_n_k * SparseMulSim_vtMv();
				double n_k_plus = jumpsPerBranch[chosenBranch_k];
				if(delta_n_k > 0.0) n_k_plus += delta_n_k;

				//first calculate term related to lambda
				double priorRatio = delta_n_k * log(lambda * tree->getBranchLength(chosenBranch_k) / n_k_plus);
				double determinantTerm = -0.5 * vars->getDataDimensionality() * log(r_k);

				//add term related to traits, summing across all dimensions
				double expTerm = vars->hastingsTerm(alpha * delta_n_k, r_k, AlphaInvMatrixData, vars->PVectorsRel, Pop, LeafIdx, StartIdx);

				/*
				std::cout << "h[" << chosenBranch_k << "] = " << h
						  << ", h2 = " << h2
						  //<< ", delta_n_k = " << delta_n_k
						  //<< ", n_k_plus = " << n_k_plus
						  << ", r_k = " << r_k
						  //<< ", alpha = " << alpha
						  //<< ", IDX = " << Pop << "|" << StartIdx << "|" << LeafIdx
						  << ", sm = " << sm << std::endl;
				*/

				double h = priorRatio + determinantTerm + expTerm;

				if(verbose) { std::cout << " (delta=" << delta_n_k << ", expTerm=" << expTerm << ", h=" << h << ", rand=" << randomForHastings[randomIndex] << ")"  << std::flush; };

				//Accept or reject move. Factor in heat of chain
				if(randomForHastings[randomIndex] < exp(heat * h)){
					updateJumpsOnBranchDB(chosenBranch_k, delta_n_k);
					double Multiplier = -alpha * delta_n_k / r_k;
					SparseMulSimAdd_MvMvt(Multiplier);
					TNAlpha_det_log += log(r_k);
					++numAccepted;
				}
				++randomIndex;
			}
		}
	}

	if(verbose) { std::cout << std::endl; };
	return numAccepted;
};

int TSigmaN::performMCMCStepForLL(const double heat, bool verbose){
	//This function performs the simple MCMC step outlined in the notes
	//The basic idea is to always only add or remove one jump on a random branch,
	//which allows us to speed up the calculation of TNAlpha
	//The MCMC for LL calculation samples from P(n|lambda)

	//per iteration, each branch will be updated once
	numAccepted = 0;
	randomIndex = 0;
	double delta_n_k;

	for(int chosenBranch_k = 0; chosenBranch_k < numBranches; ++chosenBranch_k){
		//ignore branches of length 0
		if(tree->getBranchLength(chosenBranch_k) > 0.0){

			//delta_n_k is either -1.0 or 1.0 with prob 1/2
			delta_n_k = -1.0 + 2.0 * delta_n_kBool[chosenBranch_k];

			//Calculate hastings ratio (only if move results in non-negative number jumps!)
			if(jumpsPerBranch[chosenBranch_k] + delta_n_k >= 0.0){

				//prepare iterators
				Pop = vars->PVectorsPop[chosenBranch_k];
				StartIdx = vars->PVectorsIdx[chosenBranch_k];
				LeafIdx = tree->nodes[chosenBranch_k]->LeafIdx;

				//calculate hastings
				double r_k = 1.0 + alpha * delta_n_k * SparseMulSim_vtMv();
				double n_k_plus = jumpsPerBranch[chosenBranch_k];
				if(delta_n_k > 0.0) n_k_plus += delta_n_k;

				//Only consider the term related to lambda
				double h = delta_n_k * log(lambda * tree->getBranchLength(chosenBranch_k) / n_k_plus);

				//Accept or reject move. Factor in heat of chain
				if(randomForHastings[randomIndex] < exp(heat * h)){
					updateJumpsOnBranchDB(chosenBranch_k, delta_n_k);
					double Multiplier = -alpha * delta_n_k / r_k;
					SparseMulSimAdd_MvMvt(Multiplier);
					TNAlpha_det_log += log(r_k);
					++numAccepted;
				}
				++randomIndex;
			}
		}
	}

	if(verbose) { std::cout << std::endl; };

	//throw "done!";

	return numAccepted;
};

//-------------------------------------------------------------
//TSigmaN_EM
//-------------------------------------------------------------
TSigmaN_EM::TSigmaN_EM(TTree* Tree, TVariables* Vars):TSigmaN(Tree, Vars){
	PMatrixS = NULL;
	SMultiplyCoef = 0;
};

TSigmaN_EM::TSigmaN_EM(TSigmaN_EM* other):TSigmaN(other){
	S = other->S;
	PMatrixS = S.Store();
	SMultiplyCoef = other->SMultiplyCoef;
};

void TSigmaN_EM::reset(const TlevyParams & LevyParams){
	TSigmaN::reset(LevyParams);
	S.ReSize(vars->Tau_inv.nrows());
	PMatrixS = S.Store();
	SMultiplyCoef = 0;
	S = 0.0;
};

void TSigmaN_EM::SparseMulSimAdd_MvMvt(double Multiplier){
	//version that does update PMatrixS
	int Idx;
	double ResSum;
	int i,j;
	fillPVectorAlphaTemp();

	if (SMultiplyCoef == 0){
		for(i=0;i<numLeaves;i++){
			if (PVectorAlphaTemp[i] != 0){
				ResSum = Multiplier*PVectorAlphaTemp[i];
				Idx = (i*(i+1)) >> 1;
				for(j=0;j<=i;j++){
					AlphaInvMatrixData[Idx+j] += ResSum*PVectorAlphaTemp[j];
				}
			}
		}
	} else {
		if (SMultiplyCoef == 1){
			for(i=0;i<numLeaves;i++){
				Idx = (i*(i+1)) >> 1;
				if (PVectorAlphaTemp[i] != 0){
					ResSum = Multiplier*PVectorAlphaTemp[i];
					for(j=0;j<=i;j++){
						PMatrixS[Idx+j] += AlphaInvMatrixData[Idx+j];
						AlphaInvMatrixData[Idx+j] += ResSum*PVectorAlphaTemp[j];
					}
				} else {
					for(j=0;j<=i;j++) PMatrixS[Idx+j] += AlphaInvMatrixData[Idx+j];
				}
			}
		} else {
			for(i=0;i<numLeaves;i++){
				Idx = (i*(i+1)) >> 1;
				if (PVectorAlphaTemp[i] != 0){
					ResSum = Multiplier*PVectorAlphaTemp[i];
					for(j=0;j<=i;j++){
						PMatrixS[Idx+j] += (SMultiplyCoef)*AlphaInvMatrixData[Idx+j];
						AlphaInvMatrixData[Idx+j] += ResSum*PVectorAlphaTemp[j];
					}
				} else {
					for(j=0;j<=i;j++) PMatrixS[Idx+j] += (SMultiplyCoef)*AlphaInvMatrixData[Idx+j];
				}
			}
		}
	}
	SMultiplyCoef = 0;
};

void TSigmaN_EM::fastMultipleAddMatricesSim(){
	int j = (numLeaves*numLeaves+numLeaves)/2;
	int i = j >> 4;
	j = j & 15;

	while (i--)	{
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
	}
	while (j--)	{
		*PMatrixS++ += SMultiplyCoef*(*AlphaInvMatrixData++);
	}
};

