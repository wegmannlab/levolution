/*
 * levolution_core.cpp
 *
 *  Created on: Jun 1, 2011
 *      Author: wegmannd
 */

#include "TLevolution_core.h"

//------------------------------------------------
// TEMIterationOutput
//------------------------------------------------
TEMIterationOutput::TEMIterationOutput(){
	_iteration = 0;
};

TEMIterationOutput::TEMIterationOutput(std::string & Filename, const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests){
	open(Filename, LevyParams, ConvergenceTests);
};

void TEMIterationOutput::open(std::string & Filename, const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests){
	//compile header
	std::vector<std::string> header = {"iteration"};
	LevyParams.addParameterNames(header);
	ConvergenceTests.addParameterNames(header);
	_iteration = 0;

	//open output file
	_out.open(Filename, header);
};

void TEMIterationOutput::write(const TlevyParams & LevyParams, const TConvergenceTestVector & ConvergenceTests){
	_out << ++_iteration;
	LevyParams.write(_out);
	ConvergenceTests.write(_out);
	_out.endln();
};

void TEMIterationOutput::close(){
	_out.close();
};

//------------------------------------------------
// TEMOutput
//------------------------------------------------
TEMOutput::TEMOutput(std::string & Filename, const TlevyParams & LevyParams, const TNullModelParams & NullModelParams, const TModelChoice & ModelChoice){
	//compile header
	std::vector<std::string> header = {"tree", "treeLength", "traitFile"};
	LevyParams.addParameterNames(header);
	NullModelParams.addParameterNames(header, "null_");
	ModelChoice.addParameterNames(header);

	//open output file
	_out.open(Filename, header);
};

void TEMOutput::write(const TlevyParams & LevyParams, const TNullModelParams & NullModelParams, const TModelChoice & ModelChoice, const std::string & TreeFile, double TreeLength, const std::string & TraitFile){
	_out << TreeFile << TreeLength << TraitFile;
	LevyParams.write(_out);
	NullModelParams.write(_out);
	ModelChoice.write(_out);
	_out.endln();
};

void TEMOutput::close(){
	_out.close();
};

//-------------------------------------------------------------------------------------------------------------------------
//TLevolution_core
//-------------------------------------------------------------------------------------------------------------------------
TLevolution_core::TLevolution_core(){
	//set output prefix and suffix
	outname = coretools::instances::parameters().getParameterWithDefault<std::string>("outName", "levolution");

	//initialize other variables
	tree=NULL;
	treeInitialized=false;
	vars=NULL;
	varsInitialized=false;
	maxBrownVar=-1;
	maxLambda=-1;
	minPoisProb=-1;
	maxNumJumps=-1;
	numProposals=-1;
	iterations=-1;
	maxIterations=-1;
	params=NULL;
	doSims=false;
	bestEStepWeight=-1;
	trendCheckVector = NULL;
	trendCheckInitialized = false;
	bestSigmaNvector=NULL;
};

void TLevolution_core::readNewickFromFile(std::string filename, std::string & newick){
	std::ifstream f;
	f.open(filename.c_str());
	if(!f) throw "File '" + filename + "' could not be opened!";

	std::string buf="";
	while(f.good() && !f.eof() &&  newick.find_first_of(";") == std::string::npos){
		getline(f, buf); // read the line
		newick+=buf;
	}
	f.close();
	buf.clear();

	//remove blanks and everything after ';'
	size_t a=newick.find_first_not_of(" \t");
	if(a != std::string::npos) newick.erase(0, a);
	a=newick.find_first_of(";");
	if(a != std::string::npos) newick.erase(a+1);
	a=newick.find_first_of(" \t");
	while( a != std::string::npos){
		if(a != std::string::npos) newick.erase(a,1);
		a=newick.find_first_of(" \t");
	}
};

void TLevolution_core::constructTreeFromFile(){
	//the file is assumed to contain a tree in Newick format, possibly spread over multiple lines
	//everything after the first ";" is ignored
	treeFile = coretools::instances::parameters().getParameter("tree");
	coretools::instances::logfile().listFlush("Reading phylogenetic tree from file '" + treeFile + " ...");
	std::string readNewick;
	readNewickFromFile(treeFile, readNewick);
	tree = new TTree(readNewick);
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().conclude("Read ", tree->getNumNodes(), " nodes and ", tree->getNumLeaves(), " leaves.");
	coretools::instances::logfile().conclude("Total tree length = ", tree->getTotalTreeLength());
	treeInitialized = true;
}

void TLevolution_core::simulateTree(){
	coretools::instances::logfile().startIndent("Simulating a phylogeneic tree:");
	//simulating a tree with birthright, deathrate, age and numSpecies given
	coretools::instances::logfile().startIndent("Using the following parameters:");
	double birthRate = coretools::instances::parameters().getParameterWithDefault("birthRate", 1.0);
	coretools::instances::logfile().list("birth rate = ", birthRate);
	double deathRate=coretools::instances::parameters().getParameterWithDefault("deathRate", 1.0);
	coretools::instances::logfile().list("death rate = ", deathRate);
	double age=coretools::instances::parameters().getParameterWithDefault("age", 100.0);
	coretools::instances::logfile().list("age = ", age);
	int numLeaves = coretools::instances::parameters().getParameterWithDefault("numLeaves", 100);
	coretools::instances::logfile().list("numLeaves = ", numLeaves);
	bool scale = false; double scaleTo = 1.0;
	if(coretools::instances::parameters().parameterExists("scaleTo")){
		scaleTo = coretools::instances::parameters().getParameter<double>("scaleTo");
		scale = true;
		coretools::instances::logfile().list("Will scale tree to a total length of ", scaleTo, ".");
	}
	coretools::instances::logfile().endIndent();

	//create tree
	coretools::instances::logfile().listFlush("Simulating phylogenetic tree ...");
	tree = new TTree(numLeaves, birthRate, deathRate, age);
	treeInitialized=true;
	tree->setRootDistanceToZero();
	if(scale) tree->scaleTree(scaleTo);
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().conclude("Simulated a tree with total length ", tree->getTotalTreeLength());

	//write tree
	std::string filename=outname+".tree";
	coretools::instances::logfile().listFlush("Writing tree to '" + filename + "' ...");
	std::ofstream f(filename.c_str());
	if(!f) throw "File 'tree.txt' could not be generated!";
	f << tree->getNewick() << std::endl;
	f.close();
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::performSimulations(){
	//TODO: support multidimensional traits
	coretools::instances::logfile().startIndent("Simulating Trait Values:");

	if(coretools::instances::parameters().parameterExists("tree")) constructTreeFromFile();
	else simulateTree();

	//read parameters
	std::vector<double> rootStates;
	coretools::instances::parameters().fillParameterIntoContainerRepeatIndexesWithDefault("rootState", rootStates, ',', {0.0});

	coretools::instances::logfile().startIndent("Will simulate ", rootStates.size(), " dimensional data with:");
	coretools::instances::logfile().list("Root state(s): ", coretools::str::concatenateString(rootStates, ", "), ". ('rootState')");

	std::vector<double> brownVar;
	coretools::instances::parameters().fillParameterIntoContainerRepeatIndexesWithDefault("brownVar", brownVar, ',', {1.0});
	coretools::instances::logfile().list("Brownian background rate(s): ", coretools::str::concatenateString(brownVar, ", "), ". ('brownVar')");

	if(rootStates.size() != brownVar.size()){
		throw "Unequal number of root states and Brownian background rates provided!";
	}
	double alpha=coretools::instances::parameters().getParameterWithDefault("alpha", 10.0);
	coretools::instances::logfile().list("alpha: ", alpha, ". ('alpha')");

	//num jumps or lambda?
	//simulate jumps and first data set
	if(coretools::instances::parameters().parameterExists("numJumps")){
		int numJumps=coretools::instances::parameters().getParameter<int>("numJumps");
		coretools::instances::logfile().list("Number of jumps: ", numJumps, ". ('numJumps')");
		coretools::instances::logfile().endIndent();
		coretools::instances::logfile().startIndent("Simulating trait values:");
		coretools::instances::logfile().listFlush("Simulating jumps ...");
		tree->evolveTraitLevyFixedNumJumps(rootStates[0], brownVar[0], brownVar[0]*alpha, numJumps);

	} else {
		double lambda;
		if(coretools::instances::parameters().parameterExists("lambdaScaled")){
			lambda = coretools::instances::parameters().getParameter<double>("lambdaScaled");
			coretools::instances::logfile().list("lambda * tree length: ", lambda, ". ('lambdaScaled')");
			lambda = lambda / tree->getTotalTreeLength();
		} else {
			lambda = coretools::instances::parameters().getParameterWithDefault("lambda", 2.0);
			coretools::instances::logfile().list("lambda: ", lambda, ". ('lambda')");
		}
		coretools::instances::logfile().endIndent();
		coretools::instances::logfile().startIndent("Simulating trait values:");
		coretools::instances::logfile().listFlush("Simulating jumps ...");
		tree->evoloveTraitLevy(rootStates[0], brownVar[0], brownVar[0]*alpha, lambda);
	}
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().conclude("Simulated ", tree->getNumJumps(), " poisson jumps.");


	//need to store data in tmp container
	std::vector< std::vector<double> > traits(rootStates.size());
	traits[0] = tree->getLeaveTraits();

	//simulating additional dimensions
	if(rootStates.size() > 1){
		coretools::instances::logfile().listFlush("Simulating additional data dimensions ...");

		for(size_t i=1; i < rootStates.size(); ++i){
			tree->evolveTraitLevyExisitingJumps(rootStates[i], brownVar[i], brownVar[1]* alpha);
			traits[i] = tree->getLeaveTraits();
		}
		coretools::instances::logfile().done();
	}

	//write traits to file
	coretools::instances::logfile().startIndent("Writing output:");
	std::string filename = outname + ".traits";
	coretools::instances::logfile().listFlush("Writing traits to '" + filename + "' ...");
	coretools::TOutputFile out(filename);
	for(size_t l = 0; l < tree->getNumLeaves(); ++l){
		out << tree->getLeaveName(l);
		for(size_t i = 0; i < rootStates.size(); ++i){
			out << traits[i][l];
		}
		out.endln();
	}
	out.close();
	coretools::instances::logfile().done();

	//write poisson newick?
	if(coretools::instances::parameters().parameterExists("writeJumps")){
		//write location of jumps
		filename=outname+".jumps";
		coretools::instances::logfile().listFlush("Writing jump locations to '" + filename + "' ...");
		out.open(filename.c_str());
		out << tree->getNewickJumps(); out.endln();
		out.close();
		coretools::instances::logfile().write(" done!");

		//write jump strength
		filename=outname+".strength";
		coretools::instances::logfile().listFlush("Writing jump strength to '" + filename + "' ...");
		out.open(filename.c_str());
		out << tree->getNewickValues(); out.endln();
		out.close();
		coretools::instances::logfile().write(" done!");
	}

	//compute and write statistics
	if(coretools::instances::parameters().parameterExists("stats")){
		coretools::instances::logfile().startIndent("Calculating summary statistics:");
		coretools::instances::logfile().listFlush("Calculating statistics ...");
		vars = new TVariables(*tree);
		varsInitialized = true;
		_nullModel.fit(vars);
		coretools::instances::logfile().write(" done!");

		//write stats
		filename = outname + ".stats";
		coretools::instances::logfile().listFlush("Writing statistics to '" + filename + "' ...");
		std::vector<std::string> header = {"traitMean", "traitVariance", "nullModelLL"};
		_nullModel.addParameterNames(header, "null_");
		coretools::TOutputFile out(filename, header);
		out << tree->getTraitMean() << tree->getTraitVariance() << _nullModel.logLikelihood();
		_nullModel.write(out);
		out.endln();
		coretools::instances::logfile().write(" done!");
	}
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::computeNullModelMLE(){
	coretools::instances::logfile().startIndent("Computing MLE estimates for null model:");
	_nullModel.fit(vars);
	_nullModel.report();
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::estimate(){
	//prepare estimation
	constructTreeFromFile();
	prepareEM();
	TlevyParams bestLevyParams;
	TlevyParams tmpLevyParams;

	//estimate null model and prepare model choice object
	computeNullModelMLE();
	TModelChoice modelChoice(_nullModel);

	//where to start?
	coretools::instances::logfile().startIndent("Choosing initial starting values for EM:");
	bestLevyParams.setRootStatesFromParameters(_nullModel.rootStates());
	bestLevyParams.setBrownianVariancesFromParameters(_nullModel.brownianVariances());
	bestLevyParams.setLambdaFromParameters(*tree);

	double initialLambda = bestLevyParams.lambda();
	bestLevyParams.setModelfit(_nullModel.logLikelihood(), 0.0);
	coretools::instances::logfile().endIndent();

	//open output files
	std::string filename = outname + "_perAlphaResults.txt";
	coretools::instances::logfile().list("Writing per alpha results to '" + filename + "'.");
	TEMOutput alphaOut(filename, bestLevyParams, _nullModel, modelChoice);

	bool writeEM = coretools::instances::parameters().parameterExists("writeEM");
	if(writeEM){
		coretools::instances::logfile().list("Will write each EM to a file. ('writeEM')");
	} else {
		coretools::instances::logfile().list("Will not write each EM to a file. (use 'writeEM' to write them)");
	}

	//grid search?
	if(coretools::instances::parameters().parameterExists("alpha")){
		//read alphas to work with
		std::vector<double> alphaValues;
		readAlphaString(alphaValues);
		trendCheckVector->clear();

		//grid search if more than one alpha value is given
		for(auto& alphaIt : alphaValues){
			tmpLevyParams = bestLevyParams;
			tmpLevyParams.setAlpha(alphaIt);
			if(bestLevyParams.lambda() < 0.1 / tree->getTotalTreeLength()){
				tmpLevyParams.setLambda(initialLambda);
			}
			trendCheckVector->clear();

			runEM(tmpLevyParams, modelChoice, alphaOut, writeEM);
			if(tmpLevyParams.logLikelihood() > bestLevyParams.logLikelihood()){
				bestLevyParams = tmpLevyParams;
			}
		}
	} else {
		//search using peak finder algo
		coretools::instances::logfile().startIndent("Will use peak finder algorithm to optimize alpha:");
		double log10AlphaStart = coretools::instances::parameters().getParameterWithDefault("logAlphaStart", 1.0);
		double log10Alpha = log10AlphaStart;
		bestLevyParams.setAlpha( pow(10.0, log10Alpha) );
		coretools::instances::logfile().list("Will start at alpha = 10^", log10Alpha, " = ", bestLevyParams.alpha());
		double step = coretools::instances::parameters().getParameterWithDefault("logAlphaStep", 1.0);
		coretools::instances::logfile().list("Will use initial step size of ", step);
		int numOptimizations = coretools::instances::parameters().getParameterWithDefault("numOptimizations", 5);
		coretools::instances::logfile().list("Will perform ", numOptimizations + " run of optimizations.");
		if (numOptimizations < 1.0) throw "At least one round of optimization has to be done.";
		double maxSearchRange = coretools::instances::parameters().getParameterWithDefault("logSearchRange", 3.0);
		coretools::instances::logfile().list("Will abort search if peak is not found within ", maxSearchRange, " orders of magnitude.");
		if (maxSearchRange < 1.0) throw "Search range has to be at least 1 order of magnitude";
		int counter = 0;
		coretools::instances::logfile().endIndent();


		//run first
		runEM(bestLevyParams, modelChoice, alphaOut, writeEM);

		//other params
		TlevyParams previousLevyParams(bestLevyParams);

		bool reachedUpperLimit = false;
		bool reachedLowerLimit = false;

		//now run peak finder
		bool continueLoop = true;
		while(continueLoop){
			//propose new alpha and get LL
			tmpLevyParams = previousLevyParams;
			log10Alpha += step;
			tmpLevyParams.setAlpha( pow(10.0, log10Alpha) );

			//reset lambda in case it was zero before
			if(previousLevyParams.lambda() == 0.0){
				if(bestLevyParams.lambda() == 0.0){
					tmpLevyParams.setLambda(0.01);
				} else {
					tmpLevyParams.setLambda( bestLevyParams.lambda() );
				}
			}

			//run EM
			//trendCheckVector->clear();
			trendCheckVector->resetToHalf();
			runEM(tmpLevyParams, modelChoice, alphaOut, writeEM);

			//do we continuein that direction? Check if we turn around
			if(tmpLevyParams.logLikelihood() < previousLevyParams.logLikelihood()){
				++counter;
				if(counter == numOptimizations){
					continueLoop = false;
					break;
				} else {
					step = - step / 2.718282;
				}
			} else {
				//prevent under / overflow in case alpha = 0 or alpha=inf
				if(fabs(log10Alpha - log10AlphaStart) > maxSearchRange){
					if((log10Alpha > log10AlphaStart && reachedUpperLimit) || (log10Alpha < log10AlphaStart && reachedLowerLimit)){
						coretools::instances::logfile().warning("Alpha search now more than 3 orders of magnitude away from start. Aborting search!");
						continueLoop = false;
						break;
					} else {
						step = - step;
						log10Alpha = log10AlphaStart;
						if(log10Alpha > log10AlphaStart) reachedUpperLimit = true;
						else reachedLowerLimit = true;
						coretools::instances::logfile().warning("Alpha search now more than ", maxSearchRange, " orders of magnitude away from start. Searching in other direction!");
					}
				}
			}

			//update previousLevyParams
			previousLevyParams = tmpLevyParams;

			//keep best if best
			if(bestLevyParams.logLikelihood() < tmpLevyParams.logLikelihood()){
				bestLevyParams = tmpLevyParams;
			}
		}
	}

	//write one line summary
	filename = outname + "_oneLineSummary.txt";
	coretools::instances::logfile().listFlush("Writing one line summary of best alpha value to '" + filename + "' ...");
	TEMOutput oneLineSummary(filename, bestLevyParams, _nullModel, modelChoice);
	oneLineSummary.write(bestLevyParams, _nullModel, modelChoice, treeFile, tree->getTotalTreeLength(), traitFile);
	oneLineSummary.close();
	alphaOut.close();
	coretools::instances::logfile().done();

	if(bestLevyParams.lambda() > 0.0){
		coretools::instances::logfile().startIndent("Parameters with largest log-likelihood:");
		bestLevyParams.report();
		modelChoice.performModelchoiceAndReport(bestLevyParams);
		coretools::instances::logfile().endIndent();

		if(coretools::instances::parameters().parameterExists("inferJumps")){
			estimateLocationOfJumps(bestLevyParams);
		}
	} else {
		coretools::instances::logfile().list("The EM algorithm converged at the null model.");
		coretools::instances::logfile().conclude("Null model is preferred");
	}

	if(coretools::instances::parameters().parameterExists("llSurface")){
		computeLLSurface(bestLevyParams);
	}
};

void TLevolution_core::readAlphaString(std::vector<double> & alphaValues){
	coretools::instances::logfile().startIndent("Parsing alpha values:");
	std::string alphastring = coretools::instances::parameters().getParameter("alpha");
	coretools::str::eraseAllWhiteSpaces(alphastring);

	//alpha is either a single number, a list of integers separated by commas or a range with number such as 1-10:5
	if(coretools::str::stringContainsOnly(alphastring, "1234567890.")){
		//is a single number
		double alpha = coretools::str::fromString<double>(alphastring);
		if(alpha<=0.0) throw "Alpha has to be > 0!";
		alphaValues.push_back(alpha);
	} else {
		//multiple values are given
		std::vector<std::string> s;

		//maybe a list of values separated by commas?
		if(coretools::str::stringContainsOnly(alphastring, "1234567890.,")){
			coretools::str::fillContainerFromString(alphastring, alphaValues, ',');
			//check values
			for(auto& it : alphaValues){
				if(it <= 0.0) throw "Alpha has to be > 0!";
			}
		}
		//maybe a range?
		else if(alphastring.find('-')!=std::string::npos){
			coretools::instances::logfile().list("Interpreting '" + alphastring + "' as a range");
			coretools::instances::logfile().conclude("Performing grid search over alpha.");
			//is range
			if(alphastring.find(':')==std::string::npos) throw "Number of alpha values to be used is missing in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			coretools::str::fillContainerFromString(alphastring, s, ':');
			if(s.size()!=2) throw "Number of alpha values to be used is missing in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			int numAlpha = coretools::str::convertString<int>(s[1]);
			if(numAlpha<2) throw "Number of alpha values in alpha string '"+alphastring+"' has to be at least 2 when giving a range! ";
			coretools::str::fillContainerFromString(s[0], s, '-');
			if(s.size()!=2) throw "Problem reading in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			double minAlpha = coretools::str::fromString<double>(s[0]);
			double maxAlpha = coretools::str::fromString<double>(s[1]);
			if(minAlpha>maxAlpha){
				double temp=maxAlpha;
				maxAlpha=minAlpha;
				minAlpha=temp;
			}
			if(minAlpha <= 0.0) throw "Alpha has to be > 0!";
			if(minAlpha == maxAlpha) throw "Alpha range has the same from and to values!";

			//prepare vector of alphas to be used
			bool logAlpha=coretools::instances::parameters().parameterExists("logAlpha");
			if(logAlpha){
				coretools::instances::logfile().list("Distributing Alpha values uniformly on the log scale.");
				double alphaStep=(log10(maxAlpha)-log10(minAlpha))/(numAlpha-1);
				for(int i=0; i<numAlpha; ++i)
					alphaValues.push_back(pow(10, log10(minAlpha) + i*alphaStep));
			} else {
				double alphaStep=(maxAlpha-minAlpha)/(numAlpha-1);
				for(int i=0; i<numAlpha; ++i)
					alphaValues.push_back(minAlpha + i*alphaStep);
			}
		} else throw "Problem reading in alpha string '"+alphastring+"'. Give either a single value, a comma separated list of values (1.5,2,3,4.7) or a range (2-10:4).";
	}
	if(alphaValues.size()==1) coretools::instances::logfile().list("Using alpha=", alphaValues[0]);
	else {
		coretools::instances::logfile().listFlush("Will test these alpha values: ", coretools::str::concatenateString(alphaValues, ", "));
		coretools::instances::logfile().newLine();
	}
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::estimateLocationOfJumps(){
	coretools::instances::logfile().startIndent("Estimating the location of jumps:");

	//read tree
	constructTreeFromFile();

	//read trait values and prepare variables
	traitFile = coretools::instances::parameters().getParameter<std::string>("traits");
	vars = new TVariables(*tree, traitFile);
	varsInitialized=true;

	//read parameters
	coretools::instances::logfile().startIndent("Using these levy parameters:");
	TlevyParams levyParams(vars->getDataDimensionality());
	coretools::instances::logfile().endIndent();

	//read MCMC settings
	coretools::instances::logfile().startIndent("MCMC to estimate location of jumps:");
	MCMCSettingsEB.initialize("EB");
	coretools::instances::logfile().endIndent();

	//Estimating jumps
	estimateLocationOfJumps(levyParams);
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::estimateLocationOfJumps(TlevyParams & levyParams){
	//estimate location of jumps using empirical Bayes
	coretools::instances::logfile().startIndent("Estimating location of jumps:");

	//settings
	coretools::instances::logfile().startIndent("MCMC settings:");
	if(MCMCSettings.initialized()){
		MCMCSettingsEB.initialize("EB", MCMCSettings.numNVectors(), MCMCSettings.thinning(), MCMCSettings.burninLength());
	} else {
		MCMCSettingsEB.initialize("EB");
	}

	std::string mcmcTypeEB = coretools::instances::parameters().getParameterWithDefault<std::string>("mcmcTypeEB", "standard");

	//create TMCMC object and run MCMC
	TMCMCEmpiricalBayes* mcmc;
	if(mcmcTypeEB == "standard"){
		coretools::instances::logfile().startIndent("Running a standard MCMC.");
		mcmc = new TMCMCEmpiricalBayes(tree, vars);
	} else if(mcmcTypeEB == "longJumps"){
		coretools::instances::logfile().startIndent("Running an MCMC with long jumps:");
		double probLongJump = coretools::instances::parameters().getParameterWithDefault("probLongJumps", 0.1);
		coretools::instances::logfile().list("Performing long jumps with probability ", probLongJump, ".");
		double probGeometric = coretools::instances::parameters().getParameterWithDefault("probGeometric", 0.95);
		coretools::instances::logfile().list("Geometric probability = ", probGeometric);
		mcmc = new TMCMCEmpiricalBayesLongJumps(tree, vars, probLongJump, probGeometric);
	} else if(mcmcTypeEB == "heated"){
		coretools::instances::logfile().startIndent("Running a heated set of chains:");
		int numChains = coretools::instances::parameters().getParameterWithDefault("numChains", 3);
		coretools::instances::logfile().list("Will use a total of ", numChains, " chains (", numChains-1, " heated)");
		if(numChains<2) throw "The number of chains has to be at least two!";
		double deltaT = coretools::instances::parameters().getParameterWithDefault("deltaT", 0.2);
		mcmc = new TMCMCEmpiricalBayesHeated(tree, vars, numChains, deltaT);
	} else throw "Unknown MCMC type '" + mcmcTypeEB + "'!";


	//Default: do not print intermediate files
	int intermediatePosteriorThinning =  coretools::instances::parameters().getParameterWithDefault("intermediatePosteriors", MCMCSettingsEB.totalLength() + 1);
	if(intermediatePosteriorThinning > MCMCSettings.totalLength()){
		coretools::instances::logfile().list("Will not write intermediate posterior files. (use 'intermediatePosteriors' to write them)");
	} else {
		coretools::instances::logfile().list("Will write intermediate posterior files every ", intermediatePosteriorThinning, " iterations. ('intermediatePosteriors')");
	}

	//run MCMC
	mcmc->reset(levyParams);
	mcmc->runMCMC(MCMCSettingsEB, intermediatePosteriorThinning, outname);

	//writing posterior probabilities to file
	coretools::instances::logfile().startIndent("Writing posterior probabilities:");
	mcmc->writeJumpPosteriors(outname, true);
	coretools::instances::logfile().endIndent();
	coretools::instances::logfile().endIndent();
	delete mcmc;
};

void TLevolution_core::computeLLSurface(TlevyParams & levyParams){
	//compute surface of LL for different lambda values using brute force (i.e. MCMC)
	coretools::instances::logfile().startIndent("Calculating likelihood surface for lambda:");

	//open output
	std::string filename = outname + "_llSurface.txt";
	coretools::instances::logfile().list("Will write surface to '" + filename + "'.");
	std::ofstream surface(filename.c_str());
	surface << "Lambda\tLL" << std::endl << levyParams.lambda() << "\t" << levyParams.logLikelihood() << std::endl;

	//how many lambdas?
	int numLambda = coretools::instances::parameters().getParameterWithDefault("surfacePos", 50.0) / 2;
	coretools::instances::logfile().startIndent("Will calculate LL at ", 2*numLambda, " positions.");
	std::vector<double> lambdaToTest;
	for(int i=1; i<(numLambda+1); ++i){
		lambdaToTest.push_back(levyParams.lambda() * pow(1.127304, i));
		lambdaToTest.push_back(levyParams.lambda() * pow(1.127304, -i));
	}
	std::sort(lambdaToTest.begin(), lambdaToTest.end());

	//compute null model MLE to normalize during MCMC
	computeNullModelMLE();

	//run MCMC for each lambda
	TMCMCLL mcmc(tree, vars, _nullModel.logLikelihood());
	for(auto& it : lambdaToTest){
		coretools::instances::logfile().startIndent("at lambda = ", it, ":");
		levyParams.setLambda(it);
		mcmc.reset(levyParams);
		mcmc.runMCMC(MCMCSettingsLL);
		surface << levyParams.lambda() << "\t" << levyParams.logLikelihood() << std::endl;
		coretools::instances::logfile().endIndent();
	}
	coretools::instances::logfile().endIndent();
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::estimateLLOnly(){
	coretools::instances::logfile().startIndent("Estimating LL at specifc parameter values:");

	//read tree
	constructTreeFromFile();

	//read trait values and prepare variables
	traitFile = coretools::instances::parameters().getParameter("traits");
	vars = new TVariables(*tree, traitFile);
	varsInitialized = true;

	//read parameters
	coretools::instances::logfile().startIndent("Using these levy parameters:");
	TlevyParams levyParams(vars->getDataDimensionality());
	coretools::instances::logfile().endIndent();

	//Parameters for LL MCMC
	coretools::instances::logfile().startIndent("MCMC to estimate log likelihood:");
	MCMCSettingsLL.initialize("LL", 500000);
	coretools::instances::logfile().endIndent();

	//compute null model MLE to normalize during MCMC
	computeNullModelMLE();

	//Create and run MCMC
	TMCMCLL mcmc(tree, vars, _nullModel.logLikelihood());
	mcmc.reset(levyParams);
	mcmc.runMCMC(MCMCSettingsLL);

	coretools::instances::logfile().list("Likelihood = ", levyParams.logLikelihood(), 0.0);
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::prepareEM(){
	//read trait values and prepare variables
	traitFile=coretools::instances::parameters().getParameter("traits");
	vars = new TVariables(*tree, traitFile);
	varsInitialized=true;

	//read parameters
	coretools::instances::logfile().startIndent("Reading EM / MCMC settings:");
	if(coretools::instances::parameters().parameterExists("maxIterations")){
		maxIterations=coretools::instances::parameters().getParameter<double>("maxIterations");
		if(maxIterations<1) throw "The maximum number of iterations is set < 1!";
	}
	else maxIterations = 100;
	coretools::instances::logfile().list("Will run EM for at maximum ", maxIterations, " iterations");

	//conditions to stop
	coretools::instances::logfile().startIndent("The EM will be considered converged if:");

	double trendCheckMaxT = coretools::instances::parameters().getParameterWithDefault("maxT", 2.0);
	coretools::instances::logfile().list("A trend could not be rejected with test statistics T < ", trendCheckMaxT, " for all parameters.");
	if(trendCheckMaxT<=0) throw "Threshold to reject trend T is <= 0!";

	double trendCheckMinN = coretools::instances::parameters().getParameterWithDefault("minN", 0.5);
	coretools::instances::logfile().list("Random fluctuations were asserted with test statistics N > ", trendCheckMinN, " for all parameters.");
	if(trendCheckMinN < 0.0) throw "Threshold to assert random fluctuations N < 0!";
	if(trendCheckMinN > 1.0) throw "Threshold to assert random fluctuations N > 1!";

	int numValTrendCheck = coretools::instances::parameters().getParameterWithDefault("numValCheckTrend", 16);
	coretools::instances::logfile().list("Trends and random fluctuations will be tested on the last ", numValTrendCheck, " values.");
	//if(numValTrendCheck<16) throw "Number of values to check for a trend in the EM is set < 16, which is too low for the approximation to the Student t-distribution.";
	if(numValTrendCheck<16) {//added by Pablo
		coretools::instances::logfile().list("WARNING: Number of values to check for a trend in the EM is set < 16, which is too low for the approximation to the Student t-distribution.");
	}
	coretools::instances::logfile().endIndent();

	trendCheckVector = new TConvergenceTestVector(vars->getDataDimensionality(), numValTrendCheck, trendCheckMaxT, trendCheckMinN);
	trendCheckInitialized = true;

	//Parameters for EM MCMC
	coretools::instances::logfile().startIndent("MCMC during EM algorithm:");
	MCMCSettings.initialize();
	coretools::instances::logfile().endIndent();

	//Parameters for LL MCMC
	coretools::instances::logfile().startIndent("MCMC to estimate log likelihood:");
	MCMCSettingsLL.initialize("LL", 100*MCMCSettings.numNVectors(), MCMCSettings.thinning(), MCMCSettings.burninLength());
	coretools::instances::logfile().endIndent();

	coretools::instances::logfile().endIndent();
};

void TLevolution_core::runEM(TlevyParams & LevyParams, TModelChoice & ModelChoice, TEMOutput & EMOutput, bool WriteEM){
	coretools::instances::logfile().startIndent("Running EM algorithm for alpha=", LevyParams.alpha(), ":");

	//prepare output
	TEMIterationOutput out;
	if(WriteEM){
		std::string filename = outname + "_EM_alpha_" + coretools::str::toString(LevyParams.alpha()) + ".txt";
		coretools::instances::logfile().list("Will write EM iteration to '" + filename + "'.");
		out.open(filename, LevyParams, *trendCheckVector);
	}

	//write header and starting values
	coretools::instances::logfile().startNumbering("Running EM:");
	coretools::instances::logfile().setListSymbol(" ");
	coretools::instances::logfile().listFlush("");
	coretools::instances::logfile().setDefaultSymbols();
	LevyParams.writeHeaderToLogTable();
	coretools::instances::logfile().flushFixedWidth("maxT", 7);
	coretools::instances::logfile().flushFixedWidth("minN", 7);
	coretools::instances::logfile().newLine();

	//run EM
	coretools::TTimer timer;
	TMCMC mcmc(tree, vars);
	mcmc.setProgressConclusionVerbosity(false);

	//output
	coretools::instances::logfile().overFlush("");
	coretools::instances::logfile().numberFlush("");
	LevyParams.writeToLogTable();
	coretools::instances::logfile().flushFixedWidth('-', 7);
	coretools::instances::logfile().flushFixedWidth('-', 7);
	//coretools::instances::logfile().writeFixedWidth("(" + coretools::str::to_string_with_precision(runtime, 1) + "s)", 9);
	coretools::instances::logfile().write("  (" + timer.formattedTime(coretools::TimeFormat::abbreviated) + ")");

	for(int i=0; i<maxIterations; ++i){
		timer.start();

		//run MCMC
		mcmc.reset(LevyParams);
		mcmc.runMCMC(MCMCSettings);

		//check if lambda is essentially zero: defined as < 0.01 jumps / tree
		if(LevyParams.lambda() < 0.01 / tree->getTotalTreeLength()){
			LevyParams.setLambda(0.0);
		}

		//compute trends
		trendCheckVector->add(LevyParams);

		//output
		coretools::instances::logfile().overFlush("");
		coretools::instances::logfile().numberFlush("");
		LevyParams.writeToLogTable();
		trendCheckVector->printToLog();
		coretools::instances::logfile().write("  (" + timer.formattedTime(coretools::TimeFormat::abbreviated) + ")");

		//output to file
		if(WriteEM){
			out.write(LevyParams, *trendCheckVector);
		}

		//stop iterations?
		if(LevyParams.lambda() == 0.0 || trendCheckVector->checkTrend()) break;
	}
	coretools::instances::logfile().endNumbering();

	//close file
	if(WriteEM){
		out.close();
	}

	//add step to compute LLlogLikelihood
	if(LevyParams.lambda() > 0.0){
		coretools::instances::logfile().startIndent("Estimating log-likelihood:");

		//run MCMC
		TMCMCLL mcmc(tree, vars, _nullModel.logLikelihood());
		mcmc.reset(LevyParams);
		mcmc.runMCMC(MCMCSettingsLL);

		//report on std(lambda)
		coretools::instances::logfile().list("Observed Fisher information = ", LevyParams.FisherInfo());
		if(LevyParams.FisherInfo() > 0.0){
			coretools::instances::logfile().conclude("Estimated std(lambda) = ", 1.0/sqrt(LevyParams.FisherInfo()));
		} else {
			coretools::instances::logfile().conclude("Fisher Information is <=0! Try running longer MCMC chains.");
		}
		coretools::instances::logfile().list("Poor mans std(lambda) = ", sqrt(LevyParams.lambda() / tree->getTotalTreeLength()));

		//conduct model choice
		ModelChoice.performModelchoiceAndReport(LevyParams);
		coretools::instances::logfile().endIndent();
	} else {
		coretools::instances::logfile().list("MLE is at null Model -> rejecting Lévy.");
		LevyParams.setModelfit(_nullModel.logLikelihood(), 0.0);
		ModelChoice.convergedAtNullModel();
	}

	//write results
	EMOutput.write(LevyParams, _nullModel, ModelChoice, treeFile, tree->getTotalTreeLength(), traitFile);
	coretools::instances::logfile().endIndent();
};

void TLevolution_core::performFTest(){
	coretools::instances::logfile().startIndent("Conducting F-test:");

	//read tree, trait values and prepare variables
	constructTreeFromFile();
	traitFile = coretools::instances::parameters().getParameter("traits");
	vars = new TVariables(*tree, traitFile);
	varsInitialized = true;

	if(vars->getDataDimensionality() > 1){
		throw "F-test is currently only supoorted for 1-dimensional traits.";
	}

	//read tree of jumps
	std::string jumpTreeFile = coretools::instances::parameters().getParameter("jumpTree");
	coretools::instances::logfile().list("Reading tree with jumps from file '"  + treeFile + " ...");
	std::string readNewick;
	readNewickFromFile(jumpTreeFile, readNewick);
	TTree* jumpTree=new TTree(readNewick);
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().conclude("Read ", tree->getNumNodes(), " nodes and ", tree->getNumLeaves(), " leaves.");

	//read jump threshold
	double jumpThreshold = coretools::instances::parameters().getParameterWithDefault("jumpThreshold", 0.9);
	coretools::instances::logfile().listFlush("Finding branches with jumps (threshold set to ", jumpThreshold, ") ...");

	//get list of branches with jumps
	std::vector<int> branchesWithJumps;
	for(int k=0; k < (int) jumpTree->getNumNodes(); ++k){
		if(jumpTree->getBranchLength(k) > jumpThreshold){
			branchesWithJumps.push_back(k);
		}
	}
	int K = branchesWithJumps.size();
	coretools::instances::logfile().write(" done!");
	coretools::instances::logfile().conclude("Found ", branchesWithJumps.size(), " branches with jumps.");
	if(branchesWithJumps.size() < 1){
		throw "No jumps retained!";
	}

	int numLeaves =  tree->getNumLeaves();
	if(K>(numLeaves-2)) throw "More jumps than number of leaves - 1!";

	//perform test
	vars->performFTest(branchesWithJumps);
};

void TLevolution_core::testMCMCConvergence(){
	coretools::instances::logfile().startIndent("Running MCMC for convergence testing:");

	std::string filename = outname + "_MCMC.txt";
	coretools::instances::logfile().list("Will write MCMC to '" + filename + "'.");
	std::ofstream MCMCfile(filename.c_str());

	//read tree
	constructTreeFromFile();

	//read trait values and prepare variables
	traitFile = coretools::instances::parameters().getParameter("traits");
	vars = new TVariables(*tree, traitFile);
	varsInitialized = true;

	//read parameters
	coretools::instances::logfile().startIndent("Using these levy parameters:");
	TlevyParams levyParams(vars->getDataDimensionality());
	coretools::instances::logfile().endIndent();

	//Parse MCMC parameters
	coretools::instances::logfile().startIndent("MCMC during EM algorithm:");
	MCMCSettings.initialize();
	coretools::instances::logfile().endIndent();

	//Create and run MCMC
	TMCMC mcmc(tree, vars);
	mcmc.setProgressConclusionVerbosity(true);

	mcmc.reset(levyParams);
	mcmc.runMCMCForConvergenceTesting(MCMCSettings, MCMCfile);

	//close output
	MCMCfile.close();

	coretools::instances::logfile().endIndent();
};
