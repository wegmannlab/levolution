/*
 * estimHet.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#include "coretools/Main/TMain.h"
#include "coretools/Main/TTask.h"

//---------------------------------------------------------------------------
// Includes for tasks
//---------------------------------------------------------------------------

#include "TLevolution_core.h"

//---------------------------------------------------------------------------
// Existing Tasks
//---------------------------------------------------------------------------

void addTaks(coretools::TMain & main) {
	// Tasks consist of a name and a pointer to a TTask object.
    // Use main.addRegularTask() to add a regular task (shown in list of available tasks)
	// Use main.addDebugTask()   to add a debug task (not shown in list of available tasks)

	main.addRegularTask("infer", new TTask_infer());
	main.addRegularTask("inferJumps", new TTask_inferJumps());
	main.addRegularTask("LL", new TTask_LL());
	main.addRegularTask("F-test", new TTask_FTest());
	main.addRegularTask("simulate", new TTask_simulate());
	main.addRegularTask("testMCMC", new TTask_testMCMC());
};

//---------------------------------------------------------------------------
// Existing Integration tests
//---------------------------------------------------------------------------

void addTests(coretools::TMain & main){
    // Use main.addTest to add integration tests

	// Use main.addTestsuite to add test suits
};


//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	//Create main by providing a program name, a version, link to repo and contact email
	coretools::TMain main("levolution", "0.9.5", "https://bitbucket.org/wegmannlab/levolution", "daniel.wegmann@unifr.ch");

	//add existing tasks
	addTaks(main);

	//now run program
	return main.run(argc, argv);
};
